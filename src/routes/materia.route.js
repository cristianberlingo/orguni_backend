const express = require('express')

const controller = require('../controllers/materia.controller')

const router = express.Router()

router.get('', controller.findAll)
router.get('/:id', controller.findById)
router.get('/alumnx/:id', controller.findByIdAlumnx)
router.get('/carrera/:id', controller.findByIdCarrera)
router.post('', controller.insert)
router.delete('/:id', controller.remove)
router.put('/:id', controller.update)

module.exports = router
