const evento = require("./evento.route");
const alumnx = require("./alumnx.route");
const institucion = require("./institucion.route");
const carrera = require("./carrera.route");
const materia = require("./materia.route");
const eventoTipo = require("./eventoTipo.route");
const pais = require("./pais.route")

module.exports = {
    evento,
    alumnx,
    institucion,
    carrera,
    materia,
    eventoTipo,
    pais    
}