const express = require('express')

const controller = require('../controllers/evento.controller')

const router = express.Router()

router.get('/tipos', controller.findAllEventoTipos)
router.get('', controller.findAll)
router.get('/:id', controller.findById)
router.get('/alumnx/:id', controller.findByIdAlumnx)
router.get('/materia/:id', controller.findByIdMateria)
router.post('', controller.insert)
router.delete('/:id', controller.remove)
router.put('/:id', controller.update)

module.exports = router
