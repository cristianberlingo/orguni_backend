const express = require('express')
const controller = require('../controllers/alumnx.controller')
const router = express.Router()
const multer = require('multer')
const fs = require('fs');

// SET STORAGE
var storageAvatars = multer.diskStorage({
    destination: function (req, file, cb) {
        if (!fs.existsSync()) {
            fs.mkdirSync(controller.getPathAvatars(), { recursive: true })
        }
        cb(null, controller.getPathAvatars())
    },
    filename: function (req, file, cb) {
        cb(null, 'tmp_' + req.body.alumnx_id + '.' + file.originalname.split('.').slice(-1))
    }
})

var uploadAvatars = multer({ storage: storageAvatars })

router.get('', controller.findAll)
router.get('/:id', controller.findById)
router.post('', controller.insert)
router.delete('/:id', controller.remove)
router.put('/:id', controller.update)
router.post('/uploadAvatar', uploadAvatars.single('avatar'), controller.uploadAvatar)

module.exports = router