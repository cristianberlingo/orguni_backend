const utils = require('../utils/utils')
const fs = require('fs');
const env = JSON.parse(fs.readFileSync("./src/config.json", 'utf-8')).db;
const entityToDto = (entity) => {
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = {}
        dto.idEvento = entity.id_evento
        dto.idMateria = entity.id_materia
        dto.idEventoTipo = entity.id_evento_tipo
        if (env !== 'json') {
            dto.fecha = utils.dateToString(entity.fecha);
        } else {
            dto.fecha = entity.fecha
        }
        if (entity.descripcion) {
            dto.descripcion = entity.descripcion
        }
        if (entity.evento_descripcion) {
            dto.eventoDescripcion = entity.evento_descripcion
        }
        if (entity.institucion_descripcion) {
            dto.institucionDescripcion = entity.institucion_descripcion
        }
        if (entity.carrera_descripcion) {
            dto.carreraDescripcion = entity.carrera_descripcion
        }
        if (entity.materia_descripcion) {
            dto.materiaDescripcion = entity.materia_descripcion
        }
        if (entity.evento_tipo_descripcion) {
            dto.eventoTipoDescripcion = entity.evento_tipo_descripcion
        }
        if (entity.id_institucion) {
            dto.idInstitucion = entity.id_institucion
        }
        if (entity.id_carrera) {
            dto.idCarrera = entity.id_carrera
        }
    }
    return dto;
}

const dtoToEntity = (dto) => {
    let entity = null;
    if (dto !== null && dto !== undefined) {
        entity = {}
        if (dto.idEvento) {
            entity.id_evento = dto.idEvento
        }
        entity.id_materia = dto.idMateria
        entity.id_evento_tipo = dto.idEventoTipo
        if (env !== 'json') {
            entity.fecha = utils.stringToDate(dto.fecha)
        } else {
            entity.fecha = dto.fecha
        }
        entity.descripcion = dto.descripcion
    }
    return entity;
}

module.exports = {
    entityToDto,
    dtoToEntity
}