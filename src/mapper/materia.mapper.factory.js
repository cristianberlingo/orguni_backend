const entityToDto = (entity) => {
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = {}
        dto.idMateria = entity.id_materia
        dto.idCarrera = entity.id_carrera
        if (entity.descripcion) {
            dto.descripcion = entity.descripcion
        }
        if (entity.institucion_descripcion) {
            dto.institucionDescripcion = entity.institucion_descripcion
        }
        if (entity.carrera_descripcion) {
            dto.carreraDescripcion = entity.carrera_descripcion
        }
        if (entity.materia_descripcion) {
            dto.materiaDescripcion = entity.materia_descripcion
        }
        if (entity.id_institucion) {
            dto.idInstitucion = entity.id_institucion
        }
        if (entity.id_alumnx) {
            dto.idAlumnx = entity.id_alumnx
        }
    }
    return dto;
}

const dtoToEntity = (dto) => {
    let entity = null;
    if (dto !== null && dto !== undefined) {
        entity = {}
        entity.id_materia = dto.idMateria
        entity.id_carrera = dto.idCarrera
        entity.descripcion = dto.descripcion
    }
    return entity;
}

module.exports = {
    entityToDto,
    dtoToEntity
}