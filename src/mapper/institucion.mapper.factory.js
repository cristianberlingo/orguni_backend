const utils = require('../utils/utils')
const fs = require('fs');
const env = JSON.parse(fs.readFileSync("./src/config.json", 'utf-8')).db;

const entityToDto = (entity) => {
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = {}
        dto.idInstitucion = entity.id_institucion
        dto.descripcion = entity.descripcion
        dto.idAlumnx = entity.id_alumnx
    }
    return dto;
}

const dtoToEntity = (dto) => {
    let entity = null;
    if (dto !== null && dto !== undefined) {
        entity = {}
        entity.id_institucion = dto.idInstitucion
        entity.descripcion = dto.descripcion
        entity.id_alumnx = dto.idAlumnx
    }
    return entity;
}

module.exports = {
    entityToDto,
    dtoToEntity
}