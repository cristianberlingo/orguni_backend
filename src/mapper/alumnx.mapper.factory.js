const utils = require('../utils/utils')
const fs = require('fs');
const env = JSON.parse(fs.readFileSync("./src/config.json", 'utf-8')).db;

const entityToDto = (entity) => {
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = {}
        dto.idAlumnx = entity.id_alumnx
        dto.nombre = entity.nombre
        dto.apellido = entity.apellido
        dto.email = entity.email
        dto.password = entity.password
        dto.idPais = entity.id_pais
        if (env !== 'json') {
            dto.fechaNacimiento = utils.dateToString(entity.fecha_nacimiento);
        } else {
            dto.fechaNacimiento = entity.fecha_nacimiento
        }
    }
    return dto;
}

const dtoToEntity = (dto) => {
    let entity = null;
    if (dto !== null && dto !== undefined) {
        entity = {}
        if (dto.idAlumnx) {
            entity.id_alumnx = dto.idAlumnx
        }
        entity.nombre = dto.nombre
        entity.apellido = dto.apellido
        entity.email = dto.email
        entity.password = dto.password
        entity.id_pais = dto.idPais
        if (env !== 'json') {
            entity.fecha_nacimiento = utils.stringToDate(dto.fechaNacimiento)
        } else {
            entity.fecha_nacimiento = dto.fechaNacimiento
        }
    }
    return entity;
}

module.exports = {
    entityToDto,
    dtoToEntity
}