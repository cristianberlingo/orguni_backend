const utils = require('../utils/utils')

const entityToDto = (entity) => {
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = {}
        dto.idEventoTipo = entity.id_evento_tipo
        dto.descripcion = entity.descripcion
    }
    return dto;
}

module.exports = {
    entityToDto
}