const utils = require('../utils/utils')
const fs = require('fs');
const env = JSON.parse(fs.readFileSync("./src/config.json", 'utf-8')).db;

const entityToDto = (entity) => {
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = {}
        dto.idCarrera = entity.id_carrera
        dto.idInstitucion = entity.id_institucion
        if (entity.descripcion) {
            dto.descripcion = entity.descripcion
        }
        if (entity.id_alumnx) {
            dto.idAlumnx = entity.id_alumnx
        }
        if (entity.institucion_descripcion) {
            dto.institucionDescripcion = entity.institucion_descripcion
        }
        if (entity.carrera_descripcion) {
            dto.carreraDescripcion = entity.carrera_descripcion
        }
    }
    return dto;
}

const dtoToEntity = (dto) => {
    let entity = null;
    if (dto !== null && dto !== undefined) {
        entity = {}
        entity.id_carrera = dto.idCarrera
        entity.descripcion = dto.descripcion
        entity.id_institucion = dto.idInstitucion
    }
    return entity;
}

module.exports = {
    entityToDto,
    dtoToEntity
}