const utils = require('../utils/utils')

const entityToDto = (entity) => {
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = {}
        dto.idPais = entity.id_pais
        dto.descripcion = entity.descripcion
    }
    return dto;
}

module.exports = {
    entityToDto
}