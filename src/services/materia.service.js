const dao = require('../dao/dao.factory').getMateriaDAO();
const utils = require('../utils/utils')
const _ = require('lodash')
const mapper = require('../mapper/materia.mapper.factory')
const baseService = require('./base.service')

const findAll = async () => {
    return await baseService.findAll(dao, mapper)
}

const findById = async (id) => {
    return baseService.findById(id, dao, mapper)
}

const findByIdAlumnx = async (id) => {
    const entityList = await dao.findByIdAlumnx(id);
    const dtoList = [];
    if (entityList !== null && entityList !== undefined && !_.isEmpty(entityList)) {
        for (const entity of entityList) {
            dtoList.push(mapper.entityToDto(entity))
        }
    }
    return dtoList;
}

const findByIdCarrera = async (id) => {
    const entityList = await dao.findByIdCarrera(id);
    const dtoList = [];
    if (entityList !== null && entityList !== undefined && !_.isEmpty(entityList)) {
        for (const entity of entityList) {
            dtoList.push(mapper.entityToDto(entity))
        }
    }
    return dtoList;
}



const insert = async (json) => {
    if ('idMateria' in json) {
        delete json.idMateria
    }
    return mapper.entityToDto(await dao.insert(mapper.dtoToEntity(json)));
}


const remove = async (id) => {
    const dto = await findById(id);
    if (!dto) {
        utils.errorFactory('Materia not found', 404);
    }
    await dao.remove(id);
}


const update = async (id, json) => {
    if (json.idMateria) {
        delete json.idMateria
    }
    const entity = await dao.update(id, mapper.dtoToEntity(json));
    if (entity === null || entity === undefined || entity == 0) {
        utils.errorFactory('Materia not found', 404)
    }
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdCarrera,
    insert,
    remove,
    update
}