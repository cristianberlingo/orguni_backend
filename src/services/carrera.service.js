const dao = require('../dao/dao.factory').getCarreraDAO();
const utils = require('../utils/utils')
const _ = require('lodash')
const mapper = require('../mapper/carrera.mapper.factory')
const baseService = require('./base.service')

const findAll = async () => {
    return await baseService.findAll(dao, mapper)
}

const findById = async (id) => {
    return baseService.findById(id, dao, mapper)
}

const findByIdAlumnx = async (id) => {
    const entityList = await dao.findByIdAlumnx(id);
    const dtoList = [];
    if (entityList !== null && entityList !== undefined && !_.isEmpty(entityList)) {
        for (const entity of entityList) {
            dtoList.push(mapper.entityToDto(entity))
        }
    }
    return dtoList;
}


const findByIdInstitucion = async (id) => {
    const entityList = await dao.findByIdInstitucion(id);
    const dtoList = [];
    if (entityList !== null && entityList !== undefined && !_.isEmpty(entityList)) {
        for (const entity of entityList) {
            dtoList.push(mapper.entityToDto(entity))
        }
    }
    return dtoList;
}


const insert = async (json) => {
    if ('idCarrera' in json) {
        delete json.idCarrera
    }
    return mapper.entityToDto(await dao.insert(mapper.dtoToEntity(json)));
}


const remove = async (id) => {
    const dto = await findById(id);
    if (!dto) {
        utils.errorFactory('Carrera not found', 404);
    }
    await dao.remove(id);
}

const update = async (id, json) => {
    if (json.idCarrera) {
        delete json.idCarrera
    }
    const entity = await dao.update(id, mapper.dtoToEntity(json));
    if (entity === null || entity === undefined || entity == 0) {
        utils.errorFactory('Carrera not found', 404)
    }
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdInstitucion,
    insert,
    remove,
    update
}