const dao = require('../dao/dao.factory').getPaisDAO();
const mapper = require('../mapper/pais.mapper.factory')
const baseService = require('./base.service')

const findAll = async () => {
    const dtoList = await baseService.findAll(dao, mapper)
    return dtoList
}

const findById = async (id) => {
    const dto = await baseService.findById(id, dao, mapper)
    return dto
}

module.exports = {
    findAll,
    findById
}