const dao = require('../dao/dao.factory').getAlumnxDAO();
const utils = require('../utils/utils')
const fs = require('fs')
const path = require('path');
const _ = require('lodash')
const mapper = require('../mapper/alumnx.mapper.factory')
const mail = require('./mail.service');
const baseService = require('./base.service')

const pathBaseUploadsAlumnx = './uploads/alumnx/'
const pathAvatars = pathBaseUploadsAlumnx + 'avatars/'
const extensionAvatars = '.jpg'

const findAll = async () => {
    return await baseService.findAll(dao, mapper)
}

const findById = async (id) => {
    const entity = await dao.findById(id)
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = mapper.entityToDto(entity)
        let urlFileAvatar = retrieveUrlImageAvatar(id)
            if (urlFileAvatar !== undefined && urlFileAvatar !== null)
            dto.urlFileAvatar = urlFileAvatar
    }
    return dto
}


const insert = async (json) => {
    if ('idAlumnx' in json) {
        delete json.idAlumnx
    }
    return mapper.entityToDto(await dao.insert(mapper.dtoToEntity(json)));
}



const remove = async (id) => {
    const alumnx = await findById(id);
    const avatar = '../../uploads/alumnx/avatars/' + id + '.jpg';
    if (!alumnx) {
        utils.errorFactory('Alumnx not found', 404);
    }
    if (fs.existsSync(avatar)) {
        fs.unlinkSync(avatar)

    }
    return await dao.remove(id);
}


const update = async (id, json) => {
    if (json.idAlumnx) {
        delete json.idAlumnx
    }
    const alumnxUpdated = await dao.update(id, mapper.dtoToEntity(json));
    if (alumnxUpdated === null || alumnxUpdated === undefined || alumnxUpdated == 0) {
        utils.errorFactory('Alumnx not found', 404)
    }
}


const uploadAvatar = (file) => {
    if (!file) {
        const error = new Error('Error. Archivo vacio')
        utils.errorFactory(error, 500)
    }
    else {
        fileExtension = file.path.split('.').splice(-1)[0].toLowerCase()
        if (fileExtension !== 'jpg') {
        const error = `Error. El archivo subido debe ser de un contenido con extension JPG.
            \nExtension del archivo subido ${fileExtension}`
        fs.unlinkSync(file.path)
        utils.errorFactory(error, 500)
        }
    }
    fs.renameSync(file.path, file.path.replace('tmp_', ''))
}


const getNameFileAvatar = (id) => {
    return id + extensionAvatars
}


const getPathFileAvatar = (id) => {
    return path.join(pathAvatars, getNameFileAvatar(id))
}

const retrieveUrlImageAvatar = (id) => {
    let pathFileAvatar = getPathFileAvatar(id)
    urlFileAvatar = null
    if (fs.existsSync(pathFileAvatar)) {
        urlFileAvatar = 'http://localhost:3000/' + pathFileAvatar
    }

    return urlFileAvatar

}

const getPathBaseUploadsAlumnx = () => { return pathBaseUploadsAlumnx }

const getPathAvatars = () => { return pathAvatars }

module.exports = {
    findAll,
    findById,
    insert,
    remove,
    update,
    uploadAvatar,
    getPathBaseUploadsAlumnx,
    getPathAvatars
}