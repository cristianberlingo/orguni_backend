const dao = require('../dao/dao.factory').getEventoDAO();
const utils = require('../utils/utils')
const _ = require('lodash')
const mapper = require('../mapper/evento.mapper.factory')
const mapperEventoTipo = require('../mapper/eventoTipo.mapper.factory')
const baseService = require('./base.service')

const findAll = async () => {
  return await baseService.findAll(dao, mapper)
}

const findById = async (id) => {
  return baseService.findById(id, dao, mapper)
}

const findByIdAlumnx = async (id) => {
  const entityList = await dao.findByIdAlumnx(id);
  const dtoList = [];
  if (entityList !== null && entityList !== undefined && !_.isEmpty(entityList)) {
    for (const entity of entityList) {
      dtoList.push(mapper.entityToDto(entity))
    }
  }
  return dtoList;
}



const findByIdMateria = async (id) => {
  const entityList = await dao.findByIdMateria(id);
  const dtoList = [];
  if (entityList !== null && entityList !== undefined && !_.isEmpty(entityList)) {
    for (const entity of entityList) {
      dtoList.push(mapper.entityToDto(entity))
    }
  }
  return dtoList;
}


const insert = async (json) => {
  if ('idEvento' in json) {
    delete json.idEvento
  }
  return mapper.entityToDto(await dao.insert(mapper.dtoToEntity(json)));
}


const remove = async (id) => {
  const evento = await findById(id);
  if (!evento) {
    utils.errorFactory('Evento not found', 404);
  }
  await dao.remove(id);
}


const update = async (id, json) => {
  if (json.idEvento) {
    delete json.idEvento
  }
  const entity = await dao.update(id, mapper.dtoToEntity(json));
  if (entity === null || entity === undefined || entity == 0) {
    utils.errorFactory('Evento not found', 404)
  }
}


const findAllEventoTipos = async () => {
  let entityList = await dao.findAllEventoTipos()
  let dtoList = []
  if (entityList !== null && entityList !== undefined && !_.isEmpty(entityList)) {
    for (const entity of entityList) {
      dtoList.push(mapperEventoTipo.entityToDto(entity))
    }
  }

  return dtoList
}

module.exports = {
  findAll,
  findById,
  findByIdAlumnx,
  findByIdMateria,
  insert,
  remove,
  update,
  findAllEventoTipos
}