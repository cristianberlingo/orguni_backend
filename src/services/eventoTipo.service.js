const dao = require('../dao/dao.factory').getEventoTipoDAO();
const mapper = require('../mapper/eventoTipo.mapper.factory')
const baseService = require('./base.service')

const findAll = async () => {
    return await baseService.findAll(dao, mapper)
}

const findById = async (id) => {
    return baseService.findById(id, dao, mapper)
}

module.exports = {
    findAll,
    findById
}