const _ = require('lodash')

const findAll = async (dao, mapper) => {
    const entityList = await dao.findAll();
    const dtoList = [];
    if (entityList !== null && entityList !== undefined && !_.isEmpty(entityList)) {
        for (const entity of entityList) {
            dtoList.push(mapper.entityToDto(entity))
        }
    }
    return dtoList;
}

const findById = async (id, dao, mapper) => {
    const entity = await dao.findById(id)
    let dto = null;
    if (entity !== null && entity !== undefined) {
        dto = mapper.entityToDto(entity)
    }
    return dto
}

module.exports = {
    findAll,
    findById
}