const nodemailer = require('nodemailer'); // email sender function 
const utils = require('../utils/utils')
/**
 * Envio un correo electronico, a partir del mail, asunto y mensaje solicitado.
 * @apiName sendEmail
 * @apiGroup email
 * 
  * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "mensaje": "OK"
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "materia no encontradx"
 *     }
 * 
 */
const sendEmail = async function (email, asunto, mensaje) {
    // Definimos el transporter
    const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'orguniort@gmail.com', // generated ethereal user
            pass: 'ort123456' // generated ethereal password
        }
    });

    // Definimos el email
    const mailOptions = {
        from: 'orguniort@gmail.com', // sender address
        to: email, // list of receivers
        subject: asunto, // Subject line
        html: mensaje, // plain text body
    };

    // Enviamos el email
    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            utils.errorFactory(err, 500)
        }
    })
};


module.exports = {
    sendEmail
}