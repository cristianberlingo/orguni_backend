const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const routes = require('./routes')
const { getEnv } = require('./db/connector')
app.use(bodyParser.json());
app.use(cors({ origin: "http://localhost:8080" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



app.use('/uploads', express.static('uploads'));
app.use('/evento', routes.evento)
app.use('/alumnx', routes.alumnx)
app.use('/institucion', routes.institucion)
app.use('/carrera', routes.carrera)
app.use('/materia', routes.materia)
app.use('/eventoTipo', routes.eventoTipo)
app.use('/pais', routes.pais)

app.listen(3000, () => console.info('OrgUni escuchando en el puerto 3000!'))
console.info("UTILIZANDO LA BASE DE DATOS: " + getEnv());

module.exports = {
  app
}