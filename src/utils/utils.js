const _ = require('lodash')
const dateFormat = require('dateformat');

const parseDateFromStringDDMMYYYY = (str) => {
    strArr = str.split("/");
    return new Date(strArr[2], strArr[1], strArr[0])
}

const responseErrorMessage = (res, msg = 'ERROR', cod = 500) => {
    if (!res) {
        throw "ERROR: responseErrorMessage, no response received."
    }
    res.status(cod).send(msg)
}

const handleError = (res, e) => {
    if (!res) {
        throw "ERROR: handleError, no response received."
    }
    if (!e) {
        res.sendStatus(500)
    }
    const code = e.code ? e.code : 500;
    const message = e.message ? e.message : 'ERROR';
    responseErrorMessage(res, message, code)
}

const errorFactory = (message, code) => {
    throw {
        code: code,
        message: message
    }
}

const keysToCamel = function (o) {
    if (_.isObject(o)) {
        const n = {};

        Object.keys(o)
            .forEach((k) => {
                n[toCamel(k)] = keysToCamel(o[k]);
            });

        return n;
    } else if (_.isArray(o)) {
        return o.map((i) => {
            return keysToCamel(i);
        });
    }

    return o;
};
const toCamel = (s) => {
    return s.replace(/([-_][a-z])/ig, ($1) => {
        return $1.toUpperCase()
            .replace('-', '')
            .replace('_', '');
    });
};

const keysToSnake = function (o) {
    if (_.isObject(o)) {
        const n = {};

        Object.keys(o)
            .forEach((k) => {
                n[toSnake(k)] = keysToSnake(o[k]);
            });

        return n;
    } else if (_.isArray(o)) {
        return o.map((i) => {
            return keysToSnake(i);
        });
    }

    return o;
};
const toSnake = (s) => {
    return s.replace(/\.?([A-Z])/g, function (x, y) { return "_" + y.toLowerCase() }).replace(/^_/, "")
};

const dateToString = (date) => {
    return dateFormat(date, "dd/mm/yyyy");
}
const stringToDate = (str) => {
    strArr = str.split("/");
    return strArr[2] + "-" + strArr[1] + "-" + strArr[0];
}

module.exports = {
    parseDateFromStringDDMMYYYY,
    responseErrorMessage,
    handleError,
    errorFactory,
    keysToCamel,
    toCamel,
    keysToSnake,
    toSnake,
    stringToDate,
    dateToString
}