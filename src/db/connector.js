const fs = require('fs');

const clients = {
    mysql: {
        client: 'mysql',
        connection: {
            host: '127.0.0.1',
            user: 'orguni',
            password: 'orguni',
            database: 'orguni'
        },
        debug: ['ComQueryPacket']
    },
    mssql: {
        client: 'mssql',
        connection: {
            server: 'localhost',
            user: 'sa',
            password: 'Mematas01',
            database: 'OrgUni',
        }
    }
}

const getEnv = () => {
    return JSON.parse(fs.readFileSync("./src/config.json", 'utf-8')).db
}

const getDB = () => {
    const env = getEnv()
    return env !== 'json' ? require('knex')(clients[env]) : null;
}

const getJSON = () => {
    return JSON.parse(rawdata = fs.readFileSync("./src/db/db.json", 'utf-8'));
}

module.exports = {
    getDB,
    getEnv,
    getJSON
}
