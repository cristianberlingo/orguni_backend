const utils = require('../utils/utils')
const service = require('../services/carrera.service')
const carreraValidation = require('../schemaValidation/carrera.joi')
const carreraIdValidation = require('../schemaValidation/carrera.id.joi')
const alumnxIdValidation = require('../schemaValidation/alumnx.id.joi')
const institucionIdValidation = require('../schemaValidation/institucion.id.joi')


/**
 * @api {get} /carrera/ 1. findAll : Devuelve una lista con todas las carreras.  
 * @apiName findAll
 * @apiGroup carrera
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *        [
 *           {
 *               "idCarrera": 1,
 *               "idInstitucion": 1,
 *               "descripcion": "Analista de sistemas"
 *           },
 *           {
 *               "idCarrera": 2,
 *               "idInstitucion": 2,
 *               "descripcion": "Abogacía"
 *           },
 *           {
 *               "idCarrera": 3,
 *               "idInstitucion": 3,
 *               "descripcion": "Ingeniería electrónica"
 *           },
 *           {
 *               "idCarrera": 4,
 *               "idInstitucion": 4,
 *               "descripcion": "Lic. en Enfermería"
 *           },
 *           {
 *               "idCarrera": 5,
 *               "idInstitucion": 5,
 *               "descripcion": "Medicina"
 *           },
 *           {
 *               "idCarrera": 7,
 *               "idInstitucion": 1,
 *               "descripcion": "Contabilidad"
 *           }
 *        ]
 */
const findAll = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        const dtoList = await service.findAll();
        res.json(dtoList)
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /carrera/:id 2. findById : Busca y devuelve una carrera, a partir de un ID recibido.
 * @apiName findById
 * @apiGroup carrera
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       {
 *          "idCarrera": 1,
 *          "idInstitucion": 1,
 *          "descripcion": "Analista de sistemas"
 *       }
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Carrera Not Found"
 *     }
 * 
 */
const findById = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        carreraIdValidation(req.params.id)
        const dto = await service.findById(req.params.id)
        if (!dto) {
            throw utils.errorFactory('Carrera not found', 404);
        }
        res.json(dto);
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /carrera/alumnx/:id 3. findByIdAlumnx : Busca y devuelve una carrera, a partir del ID de un alumnx
 * @apiName findByIdAlumnx
 * @apiGroup carrera
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *          [
 *            {
 *                "idCarrera": 1,
 *                "idInstitucion": 1,
 *                "idAlumnx": 1,
 *                "institucionDescripcion": "ORT",
 *                "carreraDescripcion": "Analista de sistemas"
 *            },
 *            {
 *                "idCarrera": 7,
 *                "idInstitucion": 1,
 *                "idAlumnx": 1,
 *                "institucionDescripcion": "ORT",
 *                "carreraDescripcion": "Contabilidad"
 *            }
 *          ]
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "idAlumnx" fails because ["idAlumnx" must be a number]"
 *     }
 * 
 */
const findByIdAlumnx = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        alumnxIdValidation(req.params.id)
        const dtoList = await service.findByIdAlumnx(req.params.id)
        if (!dtoList) {
            throw utils.errorFactory('Carreras not found', 404);
        }
        res.json(dtoList);
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /carrera/institucion/:id 4. findByIdInstitucion : Busca y devuelve una carrera, a partir del ID de un Institucion
 * @apiName findByIdInstitucion
 * @apiGroup carrera
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       [
 *          {
 *              "idCarrera": 1,
 *              "idInstitucion": 1,
 *              "descripcion": "Analista de sistemas"
 *          },
 *          {
 *              "idCarrera": 7,
 *              "idInstitucion": 1,
 *              "descripcion": "Contabilidad"
 *          }
 *       ]
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Carreras not found"
 *     }
 * 
 */
const findByIdInstitucion = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        institucionIdValidation(req.params.id)
        const dtoList = await service.findByIdInstitucion(req.params.id)
        if (!dtoList) {
            throw utils.errorFactory('Carreras not found', 404);
        }
        res.json(dtoList);
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * 
 * @api {put} /carrera/:id 5. insert : Se carga una nueva carrera, a partir de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName insert
 * @apiGroup carrera
 * 
 * 
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       {
 *              "idCarrera": 1,
 *              "idInstitucion": 1,
 *              "descripcion": "Las de sistemas"
 *          }
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "idInstitucion" fails because ["idInstitucion" is required]"
 *     }
 * 
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Carrera not found"
 *     }
 * 
 */
const insert = async (req, res) => {
    console.log('POST: ' + req.url);
    try {
        carreraValidation(req.body)
        res.status(201).json(await service.insert(req.body))
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {delete} /carrera/:id 6. remove: Se quita carrera de la base
 * @apiName remove
 * @apiGroup carrera
 * 
 * 
 * @apiSuccessExample Success-Response:
 *   
 *   HTTP/1.1 204 NO CONTENT
 * 
 *  @apiErrorExample Error-Response:
 * 
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Carrera not found"
 *     }
 * 
 */
const remove = async (req, res) => {
    console.log('DELETE: ' + req.url);
    try {
        carreraIdValidation(req.params.id)
        await service.remove(req.params.id);
        res.sendStatus(204);
    } catch (e) {
        utils.handleError(res, e);
        return
    }
}

/**
 * @api {put} /carrera/:id 7. update : Se carga una nueva carrera, a parti de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName update
 * @apiGroup carrera
 * 
 * 
 * @apiSuccessExample Success-Response:

 *   HTTP/1.1 204 NO CONTENT
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Carrera not found"
 *     }
 * 
 */
const update = async (req, res) => {
    console.log('PUT: ' + req.url);
    try {
        carreraIdValidation(req.params.id)
        carreraValidation(req.body)
        await service.update(req.params.id, req.body)
        res.status(200).json(req.body)
    } catch (e) {
        utils.handleError(res, e);
    }
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdInstitucion,
    insert,
    remove,
    update
}
