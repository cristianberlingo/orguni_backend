const utils = require('../utils/utils')
const service = require('../services/pais.service')
const paisValidation = require('../schemaValidation/pais.id.joi')

/**
 * 
 * @api {get} /pais/ 1. findAll : Devuelve la lista de todos los Paises en la base de datos 
 * @apiName findAll
 * @apiGroup pais
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       [
 *           {
 *               "idPais": 1,
 *               "descripcion": "Argentina"
 *           },
 *           {
 *               "idPais": 2,
 *               "descripcion": "Uruguay"
 *           },
 *           {
 *               "idPais": 3,
 *               "descripcion": "Chile"
 *           },
 *           {
 *               "idPais": 4,
 *               "descripcion": "Venezuela"
 *           },
 *           {
 *               "idPais": 5,
 *               "descripcion": "Brasil"
 *           }
 *      ]
 * 
 */
const findAll = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        let paisList = await service.findAll();
        res.json(paisList)
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /pais/:id 2. findById : Busca y devuelve un pais, a partir de un ID recibido.
 * @apiName findById
 * @apiGroup pais
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *       {
 *          "idPais": 3,
 *          "descripcion": "Chile"
 *
 *       }
 *     }
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "ERROR": "Pais not found"
 *     }
 * 
 */
const findById = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        paisValidation(req.params.id)
        let pais = await service.findById(req.params.id)
        if (!pais) {
            throw utils.errorFactory('Pais not found', 404);
        }
        res.json(pais);
    } catch (e) {
        utils.handleError(res, e);
    }
}

module.exports = {
    findAll,
    findById
}
