const utils = require('../utils/utils')
const service = require('../services/evento.service')
const eventoValidation = require('../schemaValidation/evento.joi')
const eventoIdValidation = require('../schemaValidation/evento.id.joi')
const alumnxIdValidation = require('../schemaValidation/alumnx.id.joi')
const materiaIdValidation = require('../schemaValidation/materia.id.joi')

/**
 * @api {get} /eventos/ 1 . findAll : Devuelve una lista de todos los eventos. 
 * @apiName findAll
 * @apiGroup evento
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      [
 *        {
 *            "idEvento": 1,
 *            "idMateria": 11,
 *            "idEventoTipo": 1,
 *            "fecha": "18/06/2019",
 *            "descripcion": "quasar y vue"
 *        },
 *        {
 *            "idEvento": 2,
 *            "idMateria": 11,
 *            "idEventoTipo": 2,
 *            "fecha": "24/06/2019",
 *            "descripcion": "normalizacion"
 *        },
 *        {
 *            "idEvento": 3,
 *            "idMateria": 9,
 *            "idEventoTipo": 3,
 *            "fecha": "01/07/2019",
 *            "descripcion": "lorem ipsum dolor sit amet"
 *        },
 *        {
 *            "idEvento": 7,
 *            "idMateria": 8,
 *            "idEventoTipo": 1,
 *            "fecha": "10/07/2019",
 *            "descripcion": "Asdasdasdasd."
 *        },
 *        {
 *            "idEvento": 9,
 *            "idMateria": 11,
 *            "idEventoTipo": 3,
 *            "fecha": "25/06/2019",
 *            "descripcion": "Aprobado con honores"
 *        }
 *      ]
 */
const findAll = async (req, res) => {
  console.log('GET: ' + req.url);
  try {
    let eventoList = await service.findAll();
    res.json(eventoList)
  } catch (e) {
    utils.handleError(res, e);
  }
}

/**
 * @api {get} /eventos/:id 2. findById : Busca y devuelve un evento, a partir de un ID recibido.
 * @apiName findById
 * @apiGroup evento
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       {
 *        "idEvento": 2,
 *        "idMateria": 11,
 *        "idEventoTipo": 2,
 *        "fecha": "24/06/2019",
 *        "descripcion": "normalizacion"
 *       }
 *     }
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "evento not found"
 *     }
 * 
 */
const findById = async (req, res) => {
  console.log('GET: ' + req.url);
  try {
    eventoIdValidation(req.params.id)
    let evento = await service.findById(req.params.id)
    if (!evento) {
      throw utils.errorFactory('evento not found', 404);
    }
    res.json(evento);
  } catch (e) {
    utils.handleError(res, e);
  }
}

/**
 * @api {get} /eventos/alumnx/:id 3. findByIdAlumnx : Busca y devuelve un evento, a partir del ID de un alumnx
 * @apiName findByIdAlumnx
 * @apiGroup evento
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       {
 *        [
 *          {
 *              "idEvento": 7,
 *              "idMateria": 8,
 *              "idEventoTipo": 1,
 *              "fecha": "10/07/2019",
 *              "eventoDescripcion": "Asdasdasdasd.",
 *              "institucionDescripcion": "ORT",
 *              "carreraDescripcion": "Analista de sistemas",
 *              "materiaDescripcion": "Programación 1",
 *              "eventoTipoDescripcion": "Trabajo Práctico",
 *              "idInstitucion": 1,
 *              "idCarrera": 1
 *          },
 *          {
 *              "idEvento": 3,
 *              "idMateria": 9,
 *              "idEventoTipo": 3,
 *              "fecha": "01/07/2019",
 *              "eventoDescripcion": "lorem ipsum dolor sit amet",
 *              "institucionDescripcion": "ORT",
 *              "carreraDescripcion": "Analista de sistemas",
 *              "materiaDescripcion": "Taller de programación 1",
 *              "eventoTipoDescripcion": "Final",
 *              "idInstitucion": 1,
 *              "idCarrera": 1
 *          },
 *          {
 *              "idEvento": 9,
 *              "idMateria": 11,
 *              "idEventoTipo": 3,
 *              "fecha": "25/06/2019",
 *              "eventoDescripcion": "Aprobado con honores",
 *              "institucionDescripcion": "ORT",
 *              "carreraDescripcion": "Analista de sistemas",
 *              "materiaDescripcion": "Programación de nuevas tecnologías 2",
 *              "eventoTipoDescripcion": "Final",
 *              "idInstitucion": 1,
 *              "idCarrera": 1
 *          },
 *          {
 *              "idEvento": 2,
 *              "idMateria": 11,
 *              "idEventoTipo": 2,
 *              "fecha": "24/06/2019",
 *              "eventoDescripcion": "normalizacion",
 *              "institucionDescripcion": "ORT",
 *              "carreraDescripcion": "Analista de sistemas",
 *              "materiaDescripcion": "Programación de nuevas tecnologías 2",
 *              "eventoTipoDescripcion": "Parcial",
 *              "idInstitucion": 1,
 *              "idCarrera": 1
 *          },
 *          {
 *              "idEvento": 1,
 *              "idMateria": 11,
 *              "idEventoTipo": 1,
 *              "fecha": "18/06/2019",
 *              "eventoDescripcion": "quasar y vue",
 *              "institucionDescripcion": "ORT",
 *              "carreraDescripcion": "Analista de sistemas",
 *              "materiaDescripcion": "Programación de nuevas tecnologías 2",
 *              "eventoTipoDescripcion": "Trabajo Práctico",
 *              "idInstitucion": 1,
 *              "idCarrera": 1
 *          }
 *        ]
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "[]"
 *     }
 * 
 */
const findByIdAlumnx = async (req, res) => {
  console.log('GET: ' + req.url);
  try {
    alumnxIdValidation(req.params.id)
    let eventos = await service.findByIdAlumnx(req.params.id)
    if (!eventos) {
      throw utils.errorFactory('eventos not found', 404);
    }
    res.json(eventos);
  } catch (e) {
    utils.handleError(res, e);
  }
}

/**
 * @api {get} /eventos/materia/:id 4. findByIdMateria : Busca y devuelve un evento, a partir del ID de una materia
 * @apiName findByIdMateria
 * @apiGroup evento
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *        {
 *            "idEvento": 7,
 *            "idMateria": 8,
 *            "idEventoTipo": 1,
 *            "fecha": "10/07/2019",
 *            "eventoDescripcion": "Asdasdasdasd.",
 *            "materiaDescripcion": "Programación 1",
 *            "eventoTipoDescripcion": "Trabajo Práctico"
 *        }
 *     ]
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "[]"
 *     }
 * 
 */
const findByIdMateria = async (req, res) => {
  console.log('GET: ' + req.url);
  try {
    materiaIdValidation(req.params.id)
    let eventos = await service.findByIdMateria(req.params.id)
    if (!eventos) {
      throw utils.errorFactory('eventos not found', 404);
    }
    res.json(eventos);
  } catch (e) {
    utils.handleError(res, e);
  }
}

/**
 * @api {put} /eventos/:id 5. insert : Se carga un nuevo evento, a partir de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName insert
 * @apiGroup evento
 *
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       {
 *        "idEvento": 2,
 *        "idMateria": 11,
 *        "idEventoTipo": 2,
 *        "fecha": "24/06/2036",
 *        "descripcion": "masdasdjba"
 *       }
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "idMateria" fails because ["idMateria" is required]"
 *     }
 * 
 */
const insert = async (req, res) => {
  console.log('POST: ' + req.url);
  try {
    eventoValidation(req.body)
    res.status(201).json(await service.insert(req.body))
  } catch (e) {
    utils.handleError(res, e);
  }
}

/**
 * @api {delete} /eventos/:id 6. remove : Se quita Evento de la base
 * @apiName remove
 * @apiGroup evento
 * 
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *      
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Evento not found"
 *     }
 * 
 */
const remove = async (req, res) => {
  console.log('DELETE: ' + req.url);
  try {
    eventoIdValidation(req.params.id)
    await service.remove(req.params.id);
    res.sendStatus(204);
  } catch (e) {
    utils.handleError(res, e);
    return
  }
}

/**
 * @api {put} /eventos/:id 7. update : Se carga un nuevo evento, a parti de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName update
 * @apiGroup evento
 * 
 * @apiSuccessExample Success-Response:

 *   HTTP/1.1 204 NO CONTENT
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "idMateria" fails because ["idMateria" is required]"
 *     }
 *    
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Evento not found"
 *     }
 * 
 */
const update = async (req, res) => {
  console.log('PUT: ' + req.url);
  try {
    eventoIdValidation(req.params.id)
    eventoValidation(req.body)
    await service.update(req.params.id, req.body)
    res.sendStatus(204)
  } catch (e) {
    utils.handleError(res, e);
  }
}

/**
 * @api {get} /evento_tipo/ 8. findAllEventoTipos : Devuelve todos los tipos de eventos
 * @apiName findAllEventoTipos
 * @apiGroup evento
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id_evento_tipo": 1,
 *              "descripcion": "Parcial"
 *          },
 *          {
 *              "id_evento_tipo": 2,
 *              "descripcion": "Trabajo Práctico"
 *          },
 *          {
 *              "id_evento_tipo": 3,
 *              "descripcion": "Final"
 *          }
 *     ]
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "EventoNotFound"
 *     }
 * 
 */
const findAllEventoTipos = async (req, res) => {
  try {
    let eventoTipos = await service.findAllEventoTipos()
    res.json(eventoTipos)
  } catch (e) {
    utils.handleError(res, e);
  }
}

module.exports = {
  findAll,
  findById,
  findByIdAlumnx,
  findByIdMateria,
  insert,
  remove,
  update,
  findAllEventoTipos
}
