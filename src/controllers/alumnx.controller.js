const utils = require('../utils/utils')
const service = require('../services/alumnx.service')
const alumnxValidation = require('../schemaValidation/alumnx.joi')
const alumnxIdValidation = require('../schemaValidation/alumnx.id.joi')

/**
 * Devuelve todos los alumnxs registrados.  
 * @api {get} /alumnx/ 1. findAll : Devuelve una lista con todos los alumnxs registrados.
 * @apiName findAll
 * @apiGroup alumnx
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       [
 *          {
 *              "idAlumnx": 1,
 *              "nombre": "Juan",
 *              "apellido": "Perez",
 *              "email": "smolina.sbs@gmail.com",
 *              "password": "qwer1234",
 *              "idPais": 54,
 *              "fechaNacimiento": "21/08/1989"
 *          },
 *          {
 *              "idAlumnx": 2,
 *              "nombre": "John",
 *              "apellido": "Doe",
 *              "email": "email2@gmail.com",
 *              "password": "12345",
 *              "idPais": 54,
 *              "fechaNacimiento": "31/12/1990"
 *          },
 *          {
 *              "idAlumnx": 3,
 *              "nombre": "Juan",
 *              "apellido": "Perez",
 *              "email": "email@gmail.com",
 *              "password": "12345",
 *              "idPais": 54,
 *              "fechaNacimiento": "01/01/1990"
 *          },
 *          {
 *              "idAlumnx": 4,
 *              "nombre": "John",
 *              "apellido": "Doe",
 *              "email": "email2@gmail.com",
 *              "password": "12345",
 *              "idPais": 54,
 *              "fechaNacimiento": "31/12/1990"
 *          },
 *          {
 *              "idAlumnx": 5,
 *              "nombre": "Cosme",
 *              "apellido": "Fulanito",
 *              "email": "cosmefulanito@gmail.com",
 *              "password": "123456",
 *              "idPais": 54,
 *              "fechaNacimiento": "31/12/1990"
 *          },
 *          {
 *              "idAlumnx": 6,
 *              "nombre": "Alfredo",
 *              "apellido": "Gelberg",
 *              "email": "alfred@hotmail.com",
 *              "password": "123456",
 *              "idPais": 54,
 *              "fechaNacimiento": "31/12/1990"
 *          },
 *          {
 *              "idAlumnx": 7,
 *              "nombre": "Alfredo",
 *              "apellido": "Barni",
 *              "email": "barbaraGomez@gmail.com",
 *              "password": "asd123456",
 *              "idPais": 54,
 *              "fechaNacimiento": "03/02/1992"
 *          },
 *          {
 *              "idAlumnx": 8,
 *              "nombre": "Cristian",
 *              "apellido": "Berlingo",
 *              "email": "berlingo@yahoo.com.ar",
 *              "password": "123456",
 *              "idPais": 54,
 *              "fechaNacimiento": "31/12/1990"
 *          },
 *          {
 *              "idAlumnx": 9,
 *              "nombre": "Barbara",
 *              "apellido": "Barni",
 *              "email": "bb@gmail.com",
 *              "password": "123456",
 *              "idPais": 54,
 *              "fechaNacimiento": "31/12/1990"
 *          }
 *      ]
 */
const findAll = async (req, res) => {
  console.log('GET: ' + req.url);
  try {
    let alumnxList = await service.findAll();
    res.json(alumnxList)
  } catch (e) {
    utils.handleError(res, e);
  }
}


/**
 * Busca y devuelve un alumnx, a partir de un ID recibido.
 * @api {get} /alumnx/:id 2. findById  : Busca y devuelve un alumnx, a partir de un ID recibido.
 * @apiName findById
 * @apiGroup alumnx
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "idAlumnx": 1,
 *        "nombre": "Juan",
 *        "apellido": "Perez",
 *        "email": "smolina.sbs@gmail.com",
 *        "password": "qwer1234",
 *        "idPais": 54,
 *        "fechaNacimiento": "21/08/1989",
 *        "urlFileAvatar": "http://localhost:3000/uploads\\alumnx\\avatars\\1.jpg"
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Alumnx not found"
 *     }
 * 
 */
const findById = async (req, res) => {
  console.log('GET: ' + req.url);
  try {
    alumnxIdValidation(req.params.id)
    let alumnx = await service.findById(req.params.id)
    if (!alumnx) {
      throw utils.errorFactory('Alumnx not found', 404);
    }
    res.json(alumnx);
  } catch (e) {
    utils.handleError(res, e);
  }
}

/**
 * Se carga un nuevo alumnx, a partir de los datos enviados. Se verifica que esten todos los datos cargados.
 * @api {put} /alumx/:id 3. insert  :  Se carga un nuevo alumnx, a partir de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName insert
 * @apiGroup alumnx
 * 
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "idAlumnx": 24,
 *       "nombre": "Sebastian",
 *       "apellido": "Alez",
 *       "email": "email3@gmail.com",
 *       "password": "12345",
 *       "idPais": 54,
 *       "fechaNacimiento": "12/01/1900"
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Alumnx no encontradx"
 *     }
 * 
 */
const insert = async (req, res) => {
  console.log('POST: ' + req.url);
  try {
    alumnxValidation(req.body)
    res.status(201).json(await service.insert(req.body))
  } catch (e) {
    utils.handleError(res, e);
  }
}

/**
 * Se quita alumnx de la base
 * @api {delete} /alumx/:id 4. remove  : Se quita alumnx de la base, pasando el ID del mismo
 * @apiName remove
 * @apiGroup alumnx
 * 
 * @apiSuccessExample Success-Response:
 *   
 *   HTTP/1.1 204 NO CONTENT
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       Error: delete from `alumnx` where `id_alumnx` = '1' - ER_ROW_IS_REFERENCED_2: Cannot delete or update a parent row: a foreign key constraint fails (`orguni`.`institucion`, CONSTRAINT `fk_institucion_1` FOREIGN KEY (`id_alumnx`) REFERENCES `alumnx` (`id_alumnx`) ON DELETE NO ACTION ON UPDATE NO ACTION)
 *     }
 * 
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Alumnx no encontradx"
 *     }
 * 
 */
const remove = async (req, res) => {
  console.log('DELETE: ' + req.url);
  try {
    alumnxIdValidation(req.params.id)
    await service.remove(req.params.id);
    res.sendStatus(204);
  } catch (e) {
    utils.handleError(res, e);
    return
  }
}

/**
 * Se carga un nuevo alumnx, a parti de los datos enviados. Se verifica que esten todos los datos cargados.
 * @api {put} /alumx/:id 5. update  : Se carga un nuevo alumnx, a parti de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName update
 * @apiGroup alumnx
 * 
 * @apiSuccessExample Success-Response:
 *   
 *   HTTP/1.1 204 NO CONTENT
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "fechaNacimiento" fails because ["fechaNacimiento" must be a string with one of the following formats [DD/MM/YYYY]]"
 *     }
 * 
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Alumnx no encontradx"
 *     }
 * 
 * 
 */
const update = async (req, res) => {
  console.log('PUT: ' + req.url);
  try {
    alumnxIdValidation(req.params.id)
    alumnxValidation(req.body)
    await service.update(req.params.id, req.body)
    res.sendStatus(204)
  } catch (e) {
    utils.handleError(res, e);
  }
}


/**
 * Se sube imagen del alumnx
 * @api {put} /alumx/:id 6. uploadAvatar  : Se sube imagen del alumnx
 * @apiName uploadAvatar
 * @apiGroup alumnx
 * 
 * @apiSuccessExample Success-Response:
 *
 *   HTTP/1.1 204 NO CONTENT
 * 
 *  @apiErrorExample Error-Response:
 * 
 *     NO ES UN ARCHIVO
 *     {
 *       "error": 
 *            "Error. El archivo subido debe ser de un contenido con extension JPG.
 *                       
 *             Extension del archivo subido txt"
 *     }
 */
const uploadAvatar = async (req, res, next) => {
  try {
    await service.uploadAvatar(req.file, next)
    res.sendStatus(200)
  } catch (error) {
    utils.handleError(res, error)
  }
}


const getPathBaseUploadsAlumnx = () => { return service.getPathBaseUploadsAlumnx() }


const getPathAvatars = () => { return service.getPathAvatars() }

module.exports = {
  findAll,
  findById,
  insert,
  remove,
  update,
  uploadAvatar,
  getPathBaseUploadsAlumnx,
  getPathAvatars
}
