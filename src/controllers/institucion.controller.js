const utils = require('../utils/utils')
const service = require('../services/institucion.service')
const institucionValidation = require('../schemaValidation/institucion.joi')
const institucionIdValidation = require('../schemaValidation/institucion.id.joi')
const alumnxIdValidation = require('../schemaValidation/alumnx.id.joi')

/**
 * @api {get} /institucion/ 1. findAll : Devuelve todos las instituciones.  
 * @apiName findAll
 * @apiGroup institucion
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      [
 *        {
 *            "idInstitucion": 1,
 *            "descripcion": "ORT",
 *            "idAlumnx": 1
 *        },
 *        {
 *            "idInstitucion": 2,
 *            "descripcion": "UBA",
 *            "idAlumnx": 2
 *        },
 *        {
 *            "idInstitucion": 3,
 *            "descripcion": "UTN",
 *            "idAlumnx": 5
 *        },
 *        {
 *            "idInstitucion": 4,
 *            "descripcion": "IUHI",
 *            "idAlumnx": 6
 *        },
 *        {
 *            "idInstitucion": 5,
 *            "descripcion": "USAL",
 *            "idAlumnx": 6
 *        },
 *        {
 *            "idInstitucion": 7,
 *            "descripcion": "ITBA Belgrano",
 *            "idAlumnx": 1
 *        }
 *      ]
 */
const findAll = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        const dtoList = await service.findAll();
        res.json(dtoList)
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /institucion/:id 2. findById : Busca y devuelve una institucion, a partir de un ID recibido.
 * @apiName findById
 * @apiGroup institucion
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       {
 *          "idInstitucion": 3,
 *          "descripcion": "UTN",
 *          "idAlumnx": 5
 *       }
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Institucion not found"
 *     }
 * 
 */
const findById = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        institucionIdValidation(req.params.id)
        const dto = await service.findById(req.params.id)
        if (!dto) {
            throw utils.errorFactory('Institucion not found', 404);
        }
        res.json(dto);
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /institucion/alumnx/:id 3. findByIdAlumnx : Busca y devuelve una institucion, a partir del ID de un alumnx
 * @apiName findByIdAlumnx
 * @apiGroup institucion
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *        {
 *            "idInstitucion": 1,
 *            "descripcion": "ORT",
 *            "idAlumnx": 1
 *        },
 *        {
 *            "idInstitucion": 7,
 *            "descripcion": "ITBA Belgrano",
 *            "idAlumnx": 1
 *        }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "[]"
 *     }
 * 
 */
const findByIdAlumnx = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        alumnxIdValidation(req.params.id)
        const dtoList = await service.findByIdAlumnx(req.params.id)
        if (!dtoList) {
            throw utils.errorFactory('Institucions not found', 404);
        }
        res.json(dtoList);
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {put} /institucion/:id 4. insert : Se carga una nueva institucion, a partir de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName insert
 * @apiGroup institucion
 * 
 * 
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       {
 *            "idInstitucion": 1,
 *            "descripcion": "UTN",
 *            "idAlumnx": 3
 *        }
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "idAlumnx" fails because ["idAlumnx" is required]"
 *     }
 * 
 */
const insert = async (req, res) => {
    console.log('POST: ' + req.url);
    try {
        institucionValidation(req.body)
        res.status(201).json(await service.insert(req.body))
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {delete} /institucion/:id 5. remove : Se quita institucion de la base
 * @apiName remove
 * @apiGroup institucion
 * 
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "mensaje": ""
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Institucions not found"
 *     }
 * 
 */
const remove = async (req, res) => {
    console.log('DELETE: ' + req.url);
    try {
        institucionIdValidation(req.params.id)
        await service.remove(req.params.id);
        res.sendStatus(204);
    } catch (e) {
        utils.handleError(res, e);
        return
    }
}

/**
 * @api {put} /institucion/:id 6. update : Se carga una nueva institucion, a parti de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName update
 * @apiGroup institucion
 * 
 * @apiSuccessExample Success-Response:
 *
 *   HTTP/1.1 204 NO CONTENT
 *
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "idAlumnx" fails because ["idAlumnx" is required]"
 *     }
 * 
 */
const update = async (req, res) => {
    console.log('PUT: ' + req.url);
    try {
        institucionIdValidation(req.params.id)
        institucionValidation(req.body)
        await service.update(req.params.id, req.body)
        res.sendStatus(204)
    } catch (e) {
        utils.handleError(res, e);
    }
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    insert,
    remove,
    update
}
