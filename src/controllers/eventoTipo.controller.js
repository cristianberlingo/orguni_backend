const utils = require('../utils/utils')
const service = require('../services/eventoTipo.service')
const eventoTipoValidation = require('../schemaValidation/eventoTipo.id.joi')

/**
 * @api {get} /evento_tipo/ 1. findAll : Devuelve una lista de todos los tipos de eventos. 
 * @apiName findAll
 * @apiGroup evento_tipo
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       [
 *          {
 *              "id_evento_tipo": 1,
 *              "descripcion": "Parcial"
 *          },
 *          {
 *              "id_evento_tipo": 2,
 *              "descripcion": "Trabajo Práctico"
 *          },
 *          {
 *              "id_evento_tipo": 3,
 *              "descripcion": "Final"
 *          }
 *      ]
 */
const findAll = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        let eventoTipoList = await service.findAll();
        res.json(eventoTipoList)
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /evento_tipo/:id 2 . findById : Busca y devuelve un tipo de evento, a partir de un ID recibido.
 * @apiName findById
 * @apiGroup evento_tipo
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          {
 *              "id_evento_tipo": 1,
 *              "descripcion": "Parcial"
 *          }
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "tipo evento not found"
 *     }
 * 
 */
const findById = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        eventoTipoValidation(req.params.id)
        let eventoTipo = await service.findById(req.params.id)
        if (!eventoTipo) {
            throw utils.errorFactory('tipo evento not found', 404);
        }
        res.json(eventoTipo);
    } catch (e) {
        utils.handleError(res, e);
    }
}

module.exports = {
    findAll,
    findById
}
