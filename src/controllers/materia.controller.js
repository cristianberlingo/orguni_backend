const utils = require('../utils/utils')
const service = require('../services/materia.service')
const materiaValidation = require('../schemaValidation/materia.joi')
const materiaIdValidation = require('../schemaValidation/materia.id.joi')
const alumnxIdValidation = require('../schemaValidation/alumnx.id.joi')
const carreraIdValidation = require('../schemaValidation/carrera.id.joi')

/**
 * @api {get} /materia/ 1. findAll : Devuelve todas las materias.
 * @apiName findAll
 * @apiGroup materia
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       [
 *          {
 *              "idMateria": 8,
 *              "idCarrera": 1,
 *              "descripcion": "Programación 1"
 *          },
 *          {
 *              "idMateria": 9,
 *              "idCarrera": 1,
 *              "descripcion": "Taller de programación 1"
 *          },
 *          {
 *              "idMateria": 10,
 *              "idCarrera": 1,
 *              "descripcion": "BD2"
 *          },
 *          {
 *              "idMateria": 11,
 *              "idCarrera": 1,
 *              "descripcion": "Programación de nuevas tecnologías 2"
 *          },
 *          {
 *              "idMateria": 12,
 *              "idCarrera": 2,
 *              "descripcion": "Sociedad y estado"
 *          },
 *          {
 *              "idMateria": 13,
 *              "idCarrera": 3,
 *              "descripcion": "Física 1"
 *          },
 *          {
 *              "idMateria": 14,
 *              "idCarrera": 4,
 *              "descripcion": "Anatomía"
 *          },
 *          {
 *              "idMateria": 15,
 *              "idCarrera": 5,
 *              "descripcion": "Ética Profesional"
 *          },
 *          {
 *              "idMateria": 17,
 *              "idCarrera": 1,
 *              "descripcion": "Comunicaciones Efectivas I"
 *          }
 *      ]
 */
const findAll = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        const dtoList = await service.findAll();
        res.json(dtoList)
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /materia/:id 2. findById : Busca y devuelve una materia, a partir de un ID recibido.
 * @apiName findById
 * @apiGroup materia
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *      {
 *        "idMateria": 14,
 *        "idCarrera": 4,
 *        "descripcion": "Anatomía"
 *      }
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Materias not found"
 *     }
 * 
 */
const findById = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        materiaIdValidation(req.params.id)
        const dto = await service.findById(req.params.id)
        if (!dto) {
            throw utils.errorFactory('Materia not found', 404);
        }
        res.json(dto);
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /materia/alumnx/:id 3. findByIdAlumnx : Busca y devuelve una materia, a partir del ID de un alumnx
 * @apiName findByIdAlumnx
 * @apiGroup materia
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       [
 *          {
 *              "idMateria": 10,
 *              "idCarrera": 1,
 *              "descripcion": "BD2",
 *              "idAlumnx": 1
 *          },
 *          {
 *              "idMateria": 17,
 *              "idCarrera": 1,
 *              "descripcion": "Comunicaciones Efectivas I",
 *              "idAlumnx": 1
 *          },
 *          {
 *              "idMateria": 8,
 *              "idCarrera": 1,
 *              "descripcion": "Programación 1",
 *              "idAlumnx": 1
 *          },
 *          {
 *              "idMateria": 11,
 *              "idCarrera": 1,
 *              "descripcion": "Programación de nuevas tecnologías 2",
 *              "idAlumnx": 1
 *          },
 *          {
 *              "idMateria": 9,
 *              "idCarrera": 1,
 *              "descripcion": "Taller de programación 1",
 *              "idAlumnx": 1
 *          }
 *      ]
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Materias not found"
 *     }
 * 
 */
const findByIdAlumnx = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        alumnxIdValidation(req.params.id)
        const dtoList = await service.findByIdAlumnx(req.params.id)
        if (!dtoList) {
            throw utils.errorFactory('Materias not found', 404);
        }
        res.json(dtoList);
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {get} /materia/carrera/:id 4. findByIdInstitucion : Busca y devuelve una materia, a partir del ID de una carrera
 * @apiName findByIdInstitucion
 * @apiGroup materia
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "idMateria": 12,
 *          "idCarrera": 2,
 *          "descripcion": "Sociedad y estado"
 *      }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Materias not found"
 *     }
 * 
 */
const findByIdCarrera = async (req, res) => {
    console.log('GET: ' + req.url);
    try {
        carreraIdValidation(req.params.id)
        const dtoList = await service.findByIdCarrera(req.params.id)
        if (!dtoList) {
            throw utils.errorFactory('Materias not found', 404);
        }
        res.json(dtoList);
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {put} /materia/:id 5. insert : Se carga una nueva materia, a partir de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName insert
 * @apiGroup materia
 * 
 * 
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       {
 *          "idMateria": 10,
 *          "idCarrera": 1,
 *          "descripcion": "LENGUA",
 *          "idAlumnx": 1
 *        }
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "idCarrera" fails because ["idCarrera" is required]"
 *     }
 * 
 */
const insert = async (req, res) => {
    console.log('POST: ' + req.url);
    try {
        materiaValidation(req.body)
        res.status(201).json(await service.insert(req.body))
    } catch (e) {
        utils.handleError(res, e);
    }
}

/**
 * @api {delete} /materia/:id 6. remove : Se quita materia de la base
 * @apiName remove
 * @apiGroup materia
 * 
 * 
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "mensaje": "[]"
 *   }
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Materia not found"
 *     }
 * 
 */
const remove = async (req, res) => {
    console.log('DELETE: ' + req.url);
    try {
        materiaIdValidation(req.params.id)
        await service.remove(req.params.id);
        res.sendStatus(204);
    } catch (e) {
        utils.handleError(res, e);
        return
    }
}

/**
 * @api {put} /materia/:id 7. update : Se carga una nueva materia, a parti de los datos enviados. Se verifica que esten todos los datos cargados.
 * @apiName update
 * @apiGroup materia
 * 
 * @apiSuccessExample Success-Response:
 *
 *   HTTP/1.1 204 NO CONTENT
 * 
 *  @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ValidationError: child "idCarrera" fails because ["idCarrera" is required]"
 *     }
 * 
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Materia not found"
 *     }
 * 
 * 
 */
const update = async (req, res) => {
    console.log('PUT: ' + req.url);
    try {
        materiaIdValidation(req.params.id)
        materiaValidation(req.body)
        await service.update(req.params.id, req.body)
        res.sendStatus(204)
    } catch (e) {
        utils.handleError(res, e);
    }
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdCarrera,
    insert,
    remove,
    update
}
