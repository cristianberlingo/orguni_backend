const fs = require('fs');
const alumnxJson = require('./alumnx.json.dao')
const alumnxSql = require('./alumnx.sql.dao')
const eventoJson = require('./evento.json.dao')
const eventoSql = require('./evento.sql.dao')
const institucionSql = require('./institucion.sql.dao')
const institucionJson = require('./institucion.json.dao')
const carreraSql = require('./carrera.sql.dao')
const carreraJson = require('./carrera.json.dao')
const materiaSql = require('./materia.sql.dao')
const materiaJson = require('./materia.json.dao')
const eventoTipoJson = require('./eventoTipo.json.dao')
const eventoTipoSql = require('./eventoTipo.sql.dao')
const paisJson = require('./pais.json.dao')
const paisSql = require('./pais.sql.dao')

const env = JSON.parse(fs.readFileSync("./src/config.json", 'utf-8')).db;

const getAlumnxDAO = () => {
    if (env === 'json') {
        return alumnxJson
    } else {
        return alumnxSql
    }
}

const getEventoDAO = () => {
    if (env === 'json') {
        return eventoJson
    } else {
        return eventoSql
    }
}

const getInstitucionDAO = () => {
    if (env === 'json') {
        return institucionJson
    } else {
        return institucionSql
    }
}

const getCarreraDAO = () => {
    if (env === 'json') {
        return carreraJson
    } else {
        return carreraSql
    }
}

const getMateriaDAO = () => {
    if (env === 'json') {
        return materiaJson
    } else {
        return materiaSql
    }
}

const getEventoTipoDAO = () => {
    if (env === 'json') {
        return eventoTipoJson
    } else {
        return eventoTipoSql
    }
}

const getPaisDAO = () => {
    if (env === 'json') {
        return paisJson
    } else {
        return paisSql
    }
}

module.exports = {
    getAlumnxDAO,
    getEventoDAO,
    getInstitucionDAO,
    getCarreraDAO,
    getMateriaDAO,
    getEventoTipoDAO,
    getPaisDAO
}