const {getJSON} = require('../db/connector');

const data = getJSON().evento_tipo;

const findAll = () => {
    return data;
}

const findById = (id) => {
    return data.find(e => e.id_evento_tipo == id);
}

const getNextId = () => {
    return (Math.max.apply(Math, data.map(function (o) {
        return o.id_evento_tipo;
    })) + 1);
}

module.exports = {
    findAll,
    findById,
}
