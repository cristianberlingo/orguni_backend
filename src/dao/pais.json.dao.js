const { getJSON } = require('../db/connector');

const data = getJSON().pais;

const findAll = () => {
    return data;
}

const findById = (id) => {
    return data.find(e => e.id_pais == id);
}

module.exports = {
    findAll,
    findById,
}
