const { getJSON } = require('../db/connector');
const utils = require('../utils/utils')
const data = getJSON().alumnx;

const findAll = () => {
    return data;
}

const findById = (id) => {
    return data.find(e => e.id_alumnx == id);
}

const insert = (json) => {
    json.id_alumnx = getNextId();
    data.push(json);
    return json;
}

const remove = (id) => {
    const posicion = data.findIndex(e => e.id_alumnx == id)
    if (posicion != -1) {
        data.splice(posicion, 1)
    }
}

const update = (id, json) => {
    json.id_alumnx = id;
    const posicion = data.findIndex(e => e.id_alumnx == id)
    if (posicion == -1) {
        utils.errorFactory("Alumnx no encontradx", 404)
    }
    data.splice(posicion, 1, json)
    return json
}

const getNextId = () => {
    return (Math.max.apply(Math, data.map(function (o) {
        return o.id_alumnx;
    })) + 1);
}

module.exports = {
    findAll,
    findById,
    insert,
    remove,
    update
}