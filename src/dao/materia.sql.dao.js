const { getDB } = require('../db/connector')
const utils = require('../utils/utils')
const baseDao = require('./base.sql.dao')
const db = getDB()
const table = 'materia'
const knex = require('knex')

const findAll = async () => {
    return await baseDao.findAll(db, table)
}

const findById = async (id) => {
    return await baseDao.findById(id, db, table)
}

const findByIdAlumnx = async (id) => {
    entityList = []
    await db.table(table)
        .select(knex.raw(`institucion.id_alumnx, materia.id_materia, materia.id_carrera, materia.descripcion`))
        .innerJoin('carrera', 'materia.id_carrera', '=', 'carrera.id_carrera')
        .innerJoin('institucion', 'carrera.id_institucion', '=', 'institucion.id_institucion')
        .where({ "institucion.id_alumnx": id })
        .orderBy('materia.descripcion', 'asc')
        .then((result) => {
            entityList = result
        }).catch((error) => {
            utils.errorFactory(error, 500)
        })
    return entityList
}

const findByIdCarrera = async (id) => {
    entityList = []
    await db.table(table)
        .select()
        .where({ id_carrera: id })
        .orderBy('descripcion', 'asc')
        .then((result) => {
            entityList = result
        }).catch((error) => {
            utils.errorFactory(error, 500)
        })
    return entityList
}

const insert = async (json) => {
    await db.table(table).returning('id_materia').insert(json).then((result) => {
        json.id_materia = result[0]
    }).catch((error) => {
        utils.errorFactory(error, 500)
    })
    return json
}

const remove = async (id) => {
    await db.table(table).where({ id_materia: id }).returning('id_materia').del().catch((error) => {
        utils.errorFactory(error.toString(), 500)
    })
    return id
}

const update = async (id, json) => {
    let entityUpdated = null
    await db.table(table).where({ id_materia: id }).returning('*').update(json).then((result) => {
        if (result instanceof Array) {
            entityUpdated = result[0] // la base devolvia el registro actualizado, no funciona en mysql
        } else {
            entityUpdated = result // en mysql devuelve la cantidad de filas actualizadas
        }
    }).catch((error) => {
        utils.errorFactory(error.toString(), 500)
    })
    if (entityUpdated === 1) {
        entityUpdated = await findById(id)
    }
    return entityUpdated
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdCarrera,
    insert,
    remove,
    update
}