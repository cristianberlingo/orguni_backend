const { getJSON } = require('../db/connector');

const data = getJSON().institucion;

const findAll = () => {
    return data;
}

const findById = (id) => {
    return data.find(e => e.id_institucion == id);
}

const findByIdAlumnx = (id) => {
    return data.filter(e => e.id_alumnx == id);
}

const insert = (json) => {
    json.id_institucion = getNextId();
    data.push(json);
    return json;
}

const remove = (id) => {
    const posicion = data.findIndex(e => e.id_institucion == id)
    if (posicion != -1) {
        data.splice(posicion, 1)
    }
}

const update = (id, json) => {
    json.id_institucion = id
    const posicion = data.findIndex(e => e.id_institucion == id)
    if (posicion == -1) {
        utils.errorFactory("Institucion no encontrada", 404)
    }
    data.splice(posicion, 1, json)
    return json
}

const getNextId = () => {
    return (Math.max.apply(Math, data.map(function (o) {
        return o.id_institucion;
    })) + 1);
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    insert,
    remove,
    update
}