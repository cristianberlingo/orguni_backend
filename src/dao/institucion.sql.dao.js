const { getDB } = require('../db/connector')
const utils = require('../utils/utils')
const baseDao = require('./base.sql.dao')
const db = getDB()
const table = 'institucion'

const findAll = async () => {
    return await baseDao.findAll(db, table)
}

const findById = async (id) => {
    return await baseDao.findById(id, db, table)
}

const findByIdAlumnx = async (id) => {
    entityList = []
    await db.table(table)
        .select()
        .where({ "id_alumnx": id })
        .orderBy('descripcion', 'asc')
        .then((result) => {
            entityList = result
        }).catch((error) => {
            utils.errorFactory(error, 500)
        })
    return entityList
}

const insert = async (json) => {
    await db.table(table).returning('id_institucion').insert(json).then((result) => {
        json.id_institucion = result[0]
    }).catch((error) => {
        utils.errorFactory(error, 500)
    })
    return json
}

const remove = async (id) => {
    await db.table(table).where({ id_institucion: id }).returning('id_institucion').del().catch((error) => {
        utils.errorFactory(error.toString(), 500)
    })
    return id
}

const update = async (id, json) => {
    let entityUpdated = null
    await db.table(table).where({ id_institucion: id }).returning('*').update(json).then((result) => {
        if (result instanceof Array) {
            entityUpdated = result[0] // la base devolvia el registro actualizado, no funciona en mysql
        } else {
            entityUpdated = result // en mysql devuelve la cantidad de filas actualizadas
        }
    }).catch((error) => {
        utils.errorFactory(error.toString(), 500)
    })
    if (entityUpdated === 1) {
        entityUpdated = await findById(id)
    }
    return entityUpdated
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    insert,
    remove,
    update
}