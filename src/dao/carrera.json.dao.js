const { getJSON } = require('../db/connector');
const data = getJSON().carrera;
const institucionDao = require('./institucion.json.dao')

const findAll = () => {
    return data;
}

const findById = (id) => {
    return data.find(e => e.id_carrera == id);
}

const findByIdAlumnx = (id) => {
    const institucionList = institucionDao.findByIdAlumnx(id)
    const retorno = []
    for (const institucion of institucionList) {
        const carreraEntityList = data.filter(e => e.id_institucion == institucion.id_institucion);
        for (const carreraEntity of carreraEntityList) {
            retorno.push({
                id_carrera: carreraEntity.id_carrera,
                id_institucion: institucion.id_institucion,
                id_alumnx: id,
                institucion_descripcion: institucion.descripcion,
                carrera_descripcion: carreraEntity.descripcion
            })
        }
    }
    return retorno;
}
const findByIdInstitucion = (id) => {
    return data.filter(e => e.id_institucion == id);
}

const insert = (json) => {
    json.id_carrera = getNextId();
    data.push(json);
    return json;
}

const remove = (id) => {
    const posicion = data.findIndex(e => e.id_carrera == id)
    if (posicion != -1) {
        data.splice(posicion, 1)
    }
}

const update = (id, json) => {
    json.id_carrera = id;
    const posicion = data.findIndex(e => e.id_carrera == id)
    if (posicion == -1) {
        utils.errorFactory("Carrera no encontrada", 404)
    }
    data.splice(posicion, 1, json)
    return json
}

const getNextId = () => {
    return (Math.max.apply(Math, data.map(function (o) {
        return o.id_carrera;
    })) + 1);
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdInstitucion,
    insert,
    remove,
    update
}