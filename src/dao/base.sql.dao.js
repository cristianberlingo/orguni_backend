const utils = require('../utils/utils')
const entity_key_prefix = 'id_'

const findAll = async (db, table) => {
    try {
        return db.table(table).select()
    } catch (e) {
        utils.errorFactory(error.toString(), 500)
    }
}

const findById = async (id, db, table) => {
    let entity = null
    let query_where = {} 
    query_where[entity_key_prefix + table] = id
    await db.table(table).select()
        .first()
        .where(query_where)
        .then((result) => {
            entity = result
        }).catch((error) => {
            utils.errorFactory(error, 500)
        })

    return entity
}

module.exports = {
    findAll,
    findById
}