const { getDB } = require('../db/connector')
const utils = require('../utils/utils')
const baseDao = require('./base.sql.dao')
const db = getDB()
const table = 'pais'

const findAll = async () => {
    try {
        return await baseDao.findAll(db, table)
    } catch (e) {
        utils.errorFactory(error.toString(), 500)
    }
}

const findById = async (id) => {
    try {
        return await baseDao.findById(id, db, table)
    } catch (e) {
        utils.errorFactory(error.toString(), 500)
    }
}

module.exports = {
    findAll,
    findById
}