const { getDB } = require('../db/connector')
const utils = require('../utils/utils')
const baseDao = require('./base.sql.dao')
const db = getDB()
const table = 'alumnx'

const findAll = async () => {
    return await baseDao.findAll(db, table)
}

const findById = async (id) => {
    return await baseDao.findById(id, db, table)
}

const insert = async (json) => {
    await db.table(table).returning('id_alumnx').insert(json).then((result) => {
        json.id_alumnx = result[0]
    }).catch((error) => {
        utils.errorFactory(error, 500)
    })
    return json
}

const remove = async (id) => {
    await db.table(table).where({ id_alumnx: id }).returning('id_alumnx').del().catch((error) => {
        utils.errorFactory(error.toString(), 500)
    })
    return id
}

const update = async (id, json) => {
    let entityUpdated = null
    await db.table(table).where({ id_alumnx: id }).returning('*').update(json).then((result) => {
        if (result instanceof Array) {
            entityUpdated = result[0] // la base devolvia el registro actualizado, no funciona en mysql
        } else {
            entityUpdated = result // en mysql devuelve la cantidad de filas actualizadas
        }
    }).catch((error) => {
        utils.errorFactory(error.toString(), 500)
    })
    if (entityUpdated === 1) {
        entityUpdated = await findById(id)
    }
    return entityUpdated
}

module.exports = {
    findAll,
    findById,
    insert,
    remove,
    update
}