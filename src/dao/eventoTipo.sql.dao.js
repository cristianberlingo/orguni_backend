const { getDB } = require('../db/connector')
const baseDao = require('./base.sql.dao')
const db = getDB()
const table = 'evento_tipo'

const findAll = async () => {
    return await baseDao.findAll(db, table)
}

const findById = async (id) => {
    return await baseDao.findById(id, db, table)
}

module.exports = {
    findAll,
    findById
}