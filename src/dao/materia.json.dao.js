const { getJSON } = require('../db/connector');
const data = getJSON().materia;
const carreraDao = require('./carrera.json.dao')

const findAll = () => {
    return data;
}

const findById = (id) => {
    return data.find(e => e.id_materia == id);
}

const findByIdAlumnx = (id) => {
    const carreraList = carreraDao.findByIdAlumnx(id)
    const retorno = []
    for (const carrera of carreraList) {
        const materiaEntityList = data.filter(e => e.id_carrera == carrera.id_carrera);
        for (const materiaEntity of materiaEntityList) {
            retorno.push({
                id_alumnx: id,
                id_materia: materiaEntity.id_materia,
                id_carrera: carrera.id_carrera,
                descripcion: materiaEntity.descripcion
            })
        }
    }
    return retorno;
}
const findByIdCarrera = (id) => {
    return data.filter(e => e.id_carrera == id);
}

const insert = (json) => {
    json.id_materia = getNextId();
    data.push(json);
    return json;
}

const remove = (id) => {
    const posicion = data.findIndex(e => e.id_materia == id)
    if (posicion != -1) {
        data.splice(posicion, 1)
    }
}

const update = (id, json) => {
    json.id_materia = id
    const posicion = data.findIndex(e => e.id_materia == id)
    if (posicion == -1) {
        utils.errorFactory("Materia no encontrada", 404)
    }
    data.splice(posicion, 1, json)
    return json
}

const getNextId = () => {
    return (Math.max.apply(Math, data.map(function (o) {
        return o.id_materia;
    })) + 1);
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdCarrera,
    insert,
    remove,
    update
}