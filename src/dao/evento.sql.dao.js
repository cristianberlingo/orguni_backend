const { getDB } = require('../db/connector')
const utils = require('../utils/utils')
const baseDao = require('./base.sql.dao')
const db = getDB()
const table = 'evento'
const tableEventoTipo = 'evento_tipo'
const knex = require('knex')

const findAll = async () => {
    return await baseDao.findAll(db, table)
}

const findById = async (id) => {
    return await baseDao.findById(id, db, table)
}

const findByIdAlumnx = async (id) => {
    evento = []
    await db.table(table)
        .select(knex.raw(`evento.fecha, 
                        evento.id_evento, 
                        materia.id_materia, 
                        carrera.id_carrera, 
                        institucion.id_institucion, 
                        institucion.descripcion as institucion_descripcion, 
                        carrera.descripcion as carrera_descripcion, 
                        materia.descripcion as materia_descripcion, 
                        evento.descripcion as evento_descripcion, 
                        evento_tipo.id_evento_tipo, 
                        evento_tipo.descripcion as evento_tipo_descripcion`
        ))
        .innerJoin('materia', 'evento.id_materia', '=', 'materia.id_materia')
        .innerJoin('carrera', 'materia.id_carrera', '=', 'carrera.id_carrera')
        .innerJoin('evento_tipo', 'evento.id_evento_tipo', '=', 'evento_tipo.id_evento_tipo')
        .innerJoin('institucion', 'carrera.id_institucion', '=', 'institucion.id_institucion')
        .where({ "institucion.id_alumnx": id })
        .orderBy('evento.fecha', 'desc')
        .then((result) => {
            evento = result
        }).catch((error) => {
            utils.errorFactory(error, 500)
        })
    return evento
}

const findByIdMateria = async (id) => {
    evento = []
    await db.table(table)
        .select(knex.raw(`evento.fecha, evento.id_evento, materia.id_materia, materia.descripcion as materia_descripcion, evento.descripcion as evento_descripcion, evento_tipo.id_evento_tipo, evento_tipo.descripcion as evento_tipo_descripcion`))
        .innerJoin('materia', 'evento.id_materia', '=', 'materia.id_materia')
        .innerJoin('evento_tipo', 'evento.id_evento_tipo', '=', 'evento_tipo.id_evento_tipo')
        .where({ "materia.id_materia": id })
        .orderBy('evento.fecha', 'desc')
        .then((result) => {
            evento = result
        }).catch((error) => {
            utils.errorFactory(error, 500)
        })
    return evento
}

const insert = async (json) => {
    await db.table(table).returning('id_evento').insert(json).then((result) => {
        json.id_evento = result[0]
    }).catch((error) => {
        utils.errorFactory(error, 500)
    })
    return json
}

const remove = async (id) => {
    await db.table(table).where({ id_evento: id }).returning('id_evento').del().catch((error) => {
        utils.errorFactory(error.toString(), 500)
    })
    return id
}

const update = async (id, json) => {
    let entityUpdated = null
    await db.table(table).where({ id_evento: id }).returning('*').update(json).then((result) => {
        if (result instanceof Array) {
            entityUpdated = result[0] // la base devolvia el registro actualizado, no funciona en mysql
        } else {
            entityUpdated = result // en mysql devuelve la cantidad de filas actualizadas
        }
    }).catch((error) => {
        utils.errorFactory(error.toString(), 500)
    })
    if (entityUpdated === 1) {
        entityUpdated = await findById(id)
    }
    return entityUpdated
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdMateria,
    insert,
    remove,
    update
}