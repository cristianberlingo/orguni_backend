const { getJSON } = require('../db/connector');

const data = getJSON().evento;
const institucionDAO = require('./institucion.json.dao')
const carreraDAO = require('./carrera.json.dao')
const materiaDAO = require('./materia.json.dao')
const eventoTipoDAO = require('./eventoTipo.json.dao')

const findAll = () => {
    return data;
}

const findById = (id) => {
    return data.find(e => e.id_evento == id);
}

const findByIdAlumnx = (id) => {
    const institucionEntityList = institucionDAO.findByIdAlumnx(id)
    const retorno = []
    for (const institucionEntity of institucionEntityList) {
        const carreraEntityList = carreraDAO.findByIdInstitucion(institucionEntity.id_institucion)
        for (const carreraEntity of carreraEntityList) {
            const materiaEntityList = materiaDAO.findByIdCarrera(carreraEntity.id_carrera)
            for (const materiaEntity of materiaEntityList) {
                const eventoEntityList = data.filter(e => e.id_materia == materiaEntity.id_materia);
                for (const eventoEntity of eventoEntityList) {
                    const eventoTipoEntity = eventoTipoDAO.findById(eventoEntity.id_evento_tipo)
                    retorno.push({
                        id_alumnx: id,
                        id_materia: materiaEntity.id_materia,
                        materia_descripcion: materiaEntity.descripcion,
                        id_carrera: carreraEntity.id_carrera,
                        carrera_descripcion: carreraEntity.descripcion,
                        id_institucion: institucionEntity.id_institucion,
                        institucion_descripcion: institucionEntity.descripcion,
                        fecha: eventoEntity.fecha,
                        evento_descripcion: eventoEntity.descripcion,
                        id_evento: eventoEntity.id_evento,
                        id_evento_tipo: eventoTipoEntity.id_evento_tipo,
                        evento_tipo_descripcion: eventoTipoEntity.descripcion
                    })

                }
            }
        }
    }
    return retorno;
}

const findByIdMateria = (id) => {
    const retorno = []
    const materiaEntity = materiaDAO.findById(id)
    const eventoEntityList = data.filter(e => e.id_materia == materiaEntity.id_materia);
    for (const eventoEntity of eventoEntityList) {
        const eventoTipoEntity = eventoTipoDAO.findById(eventoEntity.id_evento_tipo)
        retorno.push({
            id_materia: materiaEntity.id_materia,
            materia_descripcion: materiaEntity.descripcion,
            fecha: eventoEntity.fecha,
            evento_descripcion: eventoEntity.descripcion,
            id_evento: eventoEntity.id_evento,
            id_evento_tipo: eventoTipoEntity.id_evento_tipo,
            evento_tipo_descripcion: eventoTipoEntity.descripcion
        })

    }
    return retorno;

}

const insert = (json) => {
    json.id_evento = getNextId();
    data.push(json);
    return json;
}

const remove = (id) => {
    const posicion = data.findIndex(e => e.id_evento == id)
    if (posicion != -1) {
        data.splice(posicion, 1)
    }
}

const update = (id, json) => {
    const posicion = data.findIndex(e => e.id_evento == id)
    if (posicion != -1) {
        data.splice(posicion, 1, json)
    }
    json.id_evento
    return json
}

const getNextId = () => {
    return (Math.max.apply(Math, data.map(function (o) {
        return o.id_evento;
    })) + 1);
}

module.exports = {
    findAll,
    findById,
    findByIdAlumnx,
    findByIdMateria,
    insert,
    remove,
    update
}
