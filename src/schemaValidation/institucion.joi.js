const BaseJoi = require('@hapi/joi');
const JoiDate = require('@hapi/joi-date');
const Joi = BaseJoi.extend(JoiDate)
const validator = require('./schemaValidatorBase')
const schema = Joi.object().keys(
    {
        idInstitucion: Joi.number().integer(),
        idAlumnx: Joi.number().integer().required(),
        descripcion: Joi.string().max(45).required()
    },
)

module.exports = function institucionValidation(data) {
    validator(data, schema);
}