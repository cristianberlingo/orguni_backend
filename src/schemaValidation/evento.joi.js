const BaseJoi = require('@hapi/joi');
const JoiDate = require('@hapi/joi-date');
const Joi = BaseJoi.extend(JoiDate)
const validator = require('./schemaValidatorBase')
const eventoSchema = Joi.object().keys(
    {
        idEvento: Joi.number().integer(),
        idMateria: Joi.number().integer().required(),
        idEventoTipo: Joi.number().integer(),
        fecha: Joi.date().format('DD/MM/YYYY').required(),
        descripcion: Joi.string().max(1000).required()
    },
)

module.exports = function eventoValidation(data) {
    validator(data, eventoSchema);
}