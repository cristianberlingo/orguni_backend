const BaseJoi = require('@hapi/joi');
const JoiDate = require('@hapi/joi-date');
const Joi = BaseJoi.extend(JoiDate)
const validator = require('./schemaValidatorBase')
const regexNombre = /^[A-Za-z\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1\u00FC\u00DC\' ]/;
const alumnxSchema = Joi.object().keys(
    {
        idAlumnx: Joi.number().integer(),
        nombre: Joi.string().insensitive().regex(regexNombre).max(40).required(),
        apellido: Joi.string().insensitive().regex(regexNombre).max(40).required(),
        email: Joi.string().email().required(),
        password: Joi.string().alphanum().required(),
        fechaNacimiento: Joi.date().format('DD/MM/YYYY').less('now').required(),
        idPais: Joi.number().integer().required()
    },
)

module.exports = function alumnxValidation(data) {
    validator(data, alumnxSchema);
}