const BaseJoi = require('@hapi/joi');
const validator = require('./schemaValidatorBase')
const alumnxSchema = BaseJoi.object().keys(
    {
        idAlumnx: BaseJoi.number().integer().required()
    },
)

module.exports = function alumnxIdValidation(id) {
    validator({ idAlumnx: id }, alumnxSchema);
}