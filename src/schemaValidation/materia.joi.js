const BaseJoi = require('@hapi/joi');
const JoiDate = require('@hapi/joi-date');
const Joi = BaseJoi.extend(JoiDate)
const validator = require('./schemaValidatorBase')
const schema = Joi.object().keys(
    {
        idMateria: Joi.number().integer(),
        idCarrera: Joi.number().integer().required(),
        descripcion: Joi.string().insensitive().max(45).required()
    },
)

module.exports = function materiaValidation(data) {
    validator(data, schema);
}