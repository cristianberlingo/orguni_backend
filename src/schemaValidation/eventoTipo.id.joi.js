const BaseJoi = require('@hapi/joi');
const validator = require('./schemaValidatorBase')
const eventoTipoSchema = BaseJoi.object().keys(
    {
        idEventoTipo: BaseJoi.number().integer().required()
    },
)

module.exports = function eventoIdValidation(id) {
    validator({ idEventoTipo: id }, eventoTipoSchema);
}