const BaseJoi = require('@hapi/joi');
const validator = require('./schemaValidatorBase')
const institucionSchema = BaseJoi.object().keys(
    {
        idInstitucion: BaseJoi.number().integer().required()
    },
)

module.exports = function institucionIdValidation(id) {
    validator({ idInstitucion: id }, institucionSchema);
}