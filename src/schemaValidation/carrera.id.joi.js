const BaseJoi = require('@hapi/joi');
const validator = require('./schemaValidatorBase')
const carreraSchema = BaseJoi.object().keys(
    {
        idCarrera: BaseJoi.number().integer().required()
    },
)

module.exports = function carreraIdValidation(id) {
    validator({ idCarrera: id }, carreraSchema);
}