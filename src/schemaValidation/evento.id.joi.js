const BaseJoi = require('@hapi/joi');
const validator = require('./schemaValidatorBase')
const eventoSchema = BaseJoi.object().keys(
    {
        idEvento: BaseJoi.number().integer().required()
    },
)

module.exports = function eventoIdValidation(id) {
    validator({ idEvento: id }, eventoSchema);
}