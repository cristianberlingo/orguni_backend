const BaseJoi = require('@hapi/joi');
const JoiDate = require('@hapi/joi-date');
const utils = require('../utils/utils');
const Joi = BaseJoi.extend(JoiDate)

/**
 * Es el componente base de la validacion de esquemas y se utiliza 
 * en todas las entidades a validar
 * @param {string[]} data esquema de datos recibidos
 * @param {string[]} schema esquema JOI a validar
 * @returns {}
 */
module.exports = function schemaValidatorBase(data, schema) {
    Joi.validate(data, schema, function (err, value) {
        if (err) {
            console.error(err);
            utils.errorFactory(err.name + ': ' + err.message, 400)
        }

    })
}