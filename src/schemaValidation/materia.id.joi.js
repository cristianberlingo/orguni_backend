const BaseJoi = require('@hapi/joi');
const validator = require('./schemaValidatorBase')
const schema = BaseJoi.object().keys(
    {
        idMateria: BaseJoi.number().integer().required()
    },
)

module.exports = function materiaIdValidation(id) {
    validator({ idMateria: id }, schema);
}