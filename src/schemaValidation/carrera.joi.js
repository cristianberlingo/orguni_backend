const BaseJoi = require('@hapi/joi');
const JoiDate = require('@hapi/joi-date');
const Joi = BaseJoi.extend(JoiDate)
const validator = require('./schemaValidatorBase')
const carreraSchema = Joi.object().keys(
    {
        idCarrera: Joi.number().integer(),
        idInstitucion: Joi.number().integer().required(),
        descripcion: Joi.string().insensitive().max(45).required()
    },
)

module.exports = function carreraValidation(data) {
    validator(data, carreraSchema);
}