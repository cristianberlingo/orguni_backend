const BaseJoi = require('@hapi/joi');
const validator = require('./schemaValidatorBase')
const paisSchema = BaseJoi.object().keys(
    {
        idPais: BaseJoi.number().integer().required()
    },
)

module.exports = function paisValidation(id) {
    validator({ idPais: id }, paisSchema);
}