define({ "api": [
  {
    "type": "get",
    "url": "/alumnx/",
    "title": "1. findAll : Devuelve una lista con todos los alumnxs registrados.",
    "name": "findAll",
    "group": "alumnx",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n     {\n         \"idAlumnx\": 1,\n         \"nombre\": \"Juan\",\n         \"apellido\": \"Perez\",\n         \"email\": \"smolina.sbs@gmail.com\",\n         \"password\": \"qwer1234\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"21/08/1989\"\n     },\n     {\n         \"idAlumnx\": 2,\n         \"nombre\": \"John\",\n         \"apellido\": \"Doe\",\n         \"email\": \"email2@gmail.com\",\n         \"password\": \"12345\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"31/12/1990\"\n     },\n     {\n         \"idAlumnx\": 3,\n         \"nombre\": \"Juan\",\n         \"apellido\": \"Perez\",\n         \"email\": \"email@gmail.com\",\n         \"password\": \"12345\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"01/01/1990\"\n     },\n     {\n         \"idAlumnx\": 4,\n         \"nombre\": \"John\",\n         \"apellido\": \"Doe\",\n         \"email\": \"email2@gmail.com\",\n         \"password\": \"12345\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"31/12/1990\"\n     },\n     {\n         \"idAlumnx\": 5,\n         \"nombre\": \"Cosme\",\n         \"apellido\": \"Fulanito\",\n         \"email\": \"cosmefulanito@gmail.com\",\n         \"password\": \"123456\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"31/12/1990\"\n     },\n     {\n         \"idAlumnx\": 6,\n         \"nombre\": \"Alfredo\",\n         \"apellido\": \"Gelberg\",\n         \"email\": \"alfred@hotmail.com\",\n         \"password\": \"123456\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"31/12/1990\"\n     },\n     {\n         \"idAlumnx\": 7,\n         \"nombre\": \"Alfredo\",\n         \"apellido\": \"Barni\",\n         \"email\": \"barbaraGomez@gmail.com\",\n         \"password\": \"asd123456\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"03/02/1992\"\n     },\n     {\n         \"idAlumnx\": 8,\n         \"nombre\": \"Cristian\",\n         \"apellido\": \"Berlingo\",\n         \"email\": \"berlingo@yahoo.com.ar\",\n         \"password\": \"123456\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"31/12/1990\"\n     },\n     {\n         \"idAlumnx\": 9,\n         \"nombre\": \"Barbara\",\n         \"apellido\": \"Barni\",\n         \"email\": \"bb@gmail.com\",\n         \"password\": \"123456\",\n         \"idPais\": 54,\n         \"fechaNacimiento\": \"31/12/1990\"\n     }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/alumnx.controller.js",
    "groupTitle": "alumnx"
  },
  {
    "type": "get",
    "url": "/alumnx/:id",
    "title": "2. findById  : Busca y devuelve un alumnx, a partir de un ID recibido.",
    "name": "findById",
    "group": "alumnx",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"idAlumnx\": 1,\n   \"nombre\": \"Juan\",\n   \"apellido\": \"Perez\",\n   \"email\": \"smolina.sbs@gmail.com\",\n   \"password\": \"qwer1234\",\n   \"idPais\": 54,\n   \"fechaNacimiento\": \"21/08/1989\",\n   \"urlFileAvatar\": \"http://localhost:3000/uploads\\\\alumnx\\\\avatars\\\\1.jpg\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Alumnx not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/alumnx.controller.js",
    "groupTitle": "alumnx"
  },
  {
    "type": "put",
    "url": "/alumx/:id",
    "title": "3. insert  :  Se carga un nuevo alumnx, a partir de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "insert",
    "group": "alumnx",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"idAlumnx\": 24,\n    \"nombre\": \"Sebastian\",\n    \"apellido\": \"Alez\",\n    \"email\": \"email3@gmail.com\",\n    \"password\": \"12345\",\n    \"idPais\": 54,\n    \"fechaNacimiento\": \"12/01/1900\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Alumnx no encontradx\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/alumnx.controller.js",
    "groupTitle": "alumnx"
  },
  {
    "type": "delete",
    "url": "/alumx/:id",
    "title": "4. remove  : Se quita alumnx de la base, pasando el ID del mismo",
    "name": "remove",
    "group": "alumnx",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\nHTTP/1.1 204 NO CONTENT",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  Error: delete from `alumnx` where `id_alumnx` = '1' - ER_ROW_IS_REFERENCED_2: Cannot delete or update a parent row: a foreign key constraint fails (`orguni`.`institucion`, CONSTRAINT `fk_institucion_1` FOREIGN KEY (`id_alumnx`) REFERENCES `alumnx` (`id_alumnx`) ON DELETE NO ACTION ON UPDATE NO ACTION)\n}\n\nHTTP/1.1 404 Not Found\n{\n  \"error\": \"Alumnx no encontradx\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/alumnx.controller.js",
    "groupTitle": "alumnx"
  },
  {
    "type": "put",
    "url": "/alumx/:id",
    "title": "5. update  : Se carga un nuevo alumnx, a parti de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "update",
    "group": "alumnx",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\nHTTP/1.1 204 NO CONTENT",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"fechaNacimiento\" fails because [\"fechaNacimiento\" must be a string with one of the following formats [DD/MM/YYYY]]\"\n}\n\nHTTP/1.1 404 Not Found\n{\n  \"error\": \"Alumnx no encontradx\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/alumnx.controller.js",
    "groupTitle": "alumnx"
  },
  {
    "type": "put",
    "url": "/alumx/:id",
    "title": "6. uploadAvatar  : Se sube imagen del alumnx",
    "name": "uploadAvatar",
    "group": "alumnx",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\nHTTP/1.1 204 NO CONTENT",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "\nNO ES UN ARCHIVO\n{\n  \"error\": \n       \"Error. El archivo subido debe ser de un contenido con extension JPG.\n                  \n        Extension del archivo subido txt\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/alumnx.controller.js",
    "groupTitle": "alumnx"
  },
  {
    "type": "get",
    "url": "/carrera/",
    "title": "1. findAll : Devuelve una lista con todas las carreras.",
    "name": "findAll",
    "group": "carrera",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n   [\n      {\n          \"idCarrera\": 1,\n          \"idInstitucion\": 1,\n          \"descripcion\": \"Analista de sistemas\"\n      },\n      {\n          \"idCarrera\": 2,\n          \"idInstitucion\": 2,\n          \"descripcion\": \"Abogacía\"\n      },\n      {\n          \"idCarrera\": 3,\n          \"idInstitucion\": 3,\n          \"descripcion\": \"Ingeniería electrónica\"\n      },\n      {\n          \"idCarrera\": 4,\n          \"idInstitucion\": 4,\n          \"descripcion\": \"Lic. en Enfermería\"\n      },\n      {\n          \"idCarrera\": 5,\n          \"idInstitucion\": 5,\n          \"descripcion\": \"Medicina\"\n      },\n      {\n          \"idCarrera\": 7,\n          \"idInstitucion\": 1,\n          \"descripcion\": \"Contabilidad\"\n      }\n   ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/carrera.controller.js",
    "groupTitle": "carrera"
  },
  {
    "type": "get",
    "url": "/carrera/:id",
    "title": "2. findById : Busca y devuelve una carrera, a partir de un ID recibido.",
    "name": "findById",
    "group": "carrera",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  {\n     \"idCarrera\": 1,\n     \"idInstitucion\": 1,\n     \"descripcion\": \"Analista de sistemas\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Carrera Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/carrera.controller.js",
    "groupTitle": "carrera"
  },
  {
    "type": "get",
    "url": "/carrera/alumnx/:id",
    "title": "3. findByIdAlumnx : Busca y devuelve una carrera, a partir del ID de un alumnx",
    "name": "findByIdAlumnx",
    "group": "carrera",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n     [\n       {\n           \"idCarrera\": 1,\n           \"idInstitucion\": 1,\n           \"idAlumnx\": 1,\n           \"institucionDescripcion\": \"ORT\",\n           \"carreraDescripcion\": \"Analista de sistemas\"\n       },\n       {\n           \"idCarrera\": 7,\n           \"idInstitucion\": 1,\n           \"idAlumnx\": 1,\n           \"institucionDescripcion\": \"ORT\",\n           \"carreraDescripcion\": \"Contabilidad\"\n       }\n     ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"idAlumnx\" fails because [\"idAlumnx\" must be a number]\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/carrera.controller.js",
    "groupTitle": "carrera"
  },
  {
    "type": "get",
    "url": "/carrera/institucion/:id",
    "title": "4. findByIdInstitucion : Busca y devuelve una carrera, a partir del ID de un Institucion",
    "name": "findByIdInstitucion",
    "group": "carrera",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n     {\n         \"idCarrera\": 1,\n         \"idInstitucion\": 1,\n         \"descripcion\": \"Analista de sistemas\"\n     },\n     {\n         \"idCarrera\": 7,\n         \"idInstitucion\": 1,\n         \"descripcion\": \"Contabilidad\"\n     }\n  ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Carreras not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/carrera.controller.js",
    "groupTitle": "carrera"
  },
  {
    "type": "put",
    "url": "/carrera/:id",
    "title": "5. insert : Se carga una nueva carrera, a partir de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "insert",
    "group": "carrera",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    {\n           \"idCarrera\": 1,\n           \"idInstitucion\": 1,\n           \"descripcion\": \"Las de sistemas\"\n       }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"idInstitucion\" fails because [\"idInstitucion\" is required]\"\n}\n\nHTTP/1.1 404 Not Found\n{\n  \"error\": \"Carrera not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/carrera.controller.js",
    "groupTitle": "carrera"
  },
  {
    "type": "delete",
    "url": "/carrera/:id",
    "title": "6. remove: Se quita carrera de la base",
    "name": "remove",
    "group": "carrera",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\nHTTP/1.1 204 NO CONTENT",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 404 Not Found\n{\n  \"error\": \"Carrera not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/carrera.controller.js",
    "groupTitle": "carrera"
  },
  {
    "type": "put",
    "url": "/carrera/:id",
    "title": "7. update : Se carga una nueva carrera, a parti de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "update",
    "group": "carrera",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 NO CONTENT",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Carrera not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/carrera.controller.js",
    "groupTitle": "carrera"
  },
  {
    "name": "sendEmail",
    "group": "email",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"mensaje\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"materia no encontradx\"\n}",
          "type": "json"
        }
      ]
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "src/services/mail.service.js",
    "groupTitle": "email"
  },
  {
    "type": "get",
    "url": "/evento_tipo/",
    "title": "1. findAll : Devuelve una lista de todos los tipos de eventos.",
    "name": "findAll",
    "group": "evento_tipo",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n     {\n         \"id_evento_tipo\": 1,\n         \"descripcion\": \"Parcial\"\n     },\n     {\n         \"id_evento_tipo\": 2,\n         \"descripcion\": \"Trabajo Práctico\"\n     },\n     {\n         \"id_evento_tipo\": 3,\n         \"descripcion\": \"Final\"\n     }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/eventoTipo.controller.js",
    "groupTitle": "evento_tipo"
  },
  {
    "type": "get",
    "url": "/evento_tipo/:id",
    "title": "2 . findById : Busca y devuelve un tipo de evento, a partir de un ID recibido.",
    "name": "findById",
    "group": "evento_tipo",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     {\n         \"id_evento_tipo\": 1,\n         \"descripcion\": \"Parcial\"\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"tipo evento not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/eventoTipo.controller.js",
    "groupTitle": "evento_tipo"
  },
  {
    "type": "get",
    "url": "/eventos/",
    "title": "1 . findAll : Devuelve una lista de todos los eventos.",
    "name": "findAll",
    "group": "evento",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n [\n   {\n       \"idEvento\": 1,\n       \"idMateria\": 11,\n       \"idEventoTipo\": 1,\n       \"fecha\": \"18/06/2019\",\n       \"descripcion\": \"quasar y vue\"\n   },\n   {\n       \"idEvento\": 2,\n       \"idMateria\": 11,\n       \"idEventoTipo\": 2,\n       \"fecha\": \"24/06/2019\",\n       \"descripcion\": \"normalizacion\"\n   },\n   {\n       \"idEvento\": 3,\n       \"idMateria\": 9,\n       \"idEventoTipo\": 3,\n       \"fecha\": \"01/07/2019\",\n       \"descripcion\": \"lorem ipsum dolor sit amet\"\n   },\n   {\n       \"idEvento\": 7,\n       \"idMateria\": 8,\n       \"idEventoTipo\": 1,\n       \"fecha\": \"10/07/2019\",\n       \"descripcion\": \"Asdasdasdasd.\"\n   },\n   {\n       \"idEvento\": 9,\n       \"idMateria\": 11,\n       \"idEventoTipo\": 3,\n       \"fecha\": \"25/06/2019\",\n       \"descripcion\": \"Aprobado con honores\"\n   }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/evento.controller.js",
    "groupTitle": "evento"
  },
  {
    "type": "get",
    "url": "/evento_tipo/",
    "title": "8. findAllEventoTipos : Devuelve todos los tipos de eventos",
    "name": "findAllEventoTipos",
    "group": "evento",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id_evento_tipo\": 1,\n         \"descripcion\": \"Parcial\"\n     },\n     {\n         \"id_evento_tipo\": 2,\n         \"descripcion\": \"Trabajo Práctico\"\n     },\n     {\n         \"id_evento_tipo\": 3,\n         \"descripcion\": \"Final\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"EventoNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/evento.controller.js",
    "groupTitle": "evento"
  },
  {
    "type": "get",
    "url": "/eventos/:id",
    "title": "2. findById : Busca y devuelve un evento, a partir de un ID recibido.",
    "name": "findById",
    "group": "evento",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  {\n   \"idEvento\": 2,\n   \"idMateria\": 11,\n   \"idEventoTipo\": 2,\n   \"fecha\": \"24/06/2019\",\n   \"descripcion\": \"normalizacion\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"evento not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/evento.controller.js",
    "groupTitle": "evento"
  },
  {
    "type": "get",
    "url": "/eventos/alumnx/:id",
    "title": "3. findByIdAlumnx : Busca y devuelve un evento, a partir del ID de un alumnx",
    "name": "findByIdAlumnx",
    "group": "evento",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n   [\n     {\n         \"idEvento\": 7,\n         \"idMateria\": 8,\n         \"idEventoTipo\": 1,\n         \"fecha\": \"10/07/2019\",\n         \"eventoDescripcion\": \"Asdasdasdasd.\",\n         \"institucionDescripcion\": \"ORT\",\n         \"carreraDescripcion\": \"Analista de sistemas\",\n         \"materiaDescripcion\": \"Programación 1\",\n         \"eventoTipoDescripcion\": \"Trabajo Práctico\",\n         \"idInstitucion\": 1,\n         \"idCarrera\": 1\n     },\n     {\n         \"idEvento\": 3,\n         \"idMateria\": 9,\n         \"idEventoTipo\": 3,\n         \"fecha\": \"01/07/2019\",\n         \"eventoDescripcion\": \"lorem ipsum dolor sit amet\",\n         \"institucionDescripcion\": \"ORT\",\n         \"carreraDescripcion\": \"Analista de sistemas\",\n         \"materiaDescripcion\": \"Taller de programación 1\",\n         \"eventoTipoDescripcion\": \"Final\",\n         \"idInstitucion\": 1,\n         \"idCarrera\": 1\n     },\n     {\n         \"idEvento\": 9,\n         \"idMateria\": 11,\n         \"idEventoTipo\": 3,\n         \"fecha\": \"25/06/2019\",\n         \"eventoDescripcion\": \"Aprobado con honores\",\n         \"institucionDescripcion\": \"ORT\",\n         \"carreraDescripcion\": \"Analista de sistemas\",\n         \"materiaDescripcion\": \"Programación de nuevas tecnologías 2\",\n         \"eventoTipoDescripcion\": \"Final\",\n         \"idInstitucion\": 1,\n         \"idCarrera\": 1\n     },\n     {\n         \"idEvento\": 2,\n         \"idMateria\": 11,\n         \"idEventoTipo\": 2,\n         \"fecha\": \"24/06/2019\",\n         \"eventoDescripcion\": \"normalizacion\",\n         \"institucionDescripcion\": \"ORT\",\n         \"carreraDescripcion\": \"Analista de sistemas\",\n         \"materiaDescripcion\": \"Programación de nuevas tecnologías 2\",\n         \"eventoTipoDescripcion\": \"Parcial\",\n         \"idInstitucion\": 1,\n         \"idCarrera\": 1\n     },\n     {\n         \"idEvento\": 1,\n         \"idMateria\": 11,\n         \"idEventoTipo\": 1,\n         \"fecha\": \"18/06/2019\",\n         \"eventoDescripcion\": \"quasar y vue\",\n         \"institucionDescripcion\": \"ORT\",\n         \"carreraDescripcion\": \"Analista de sistemas\",\n         \"materiaDescripcion\": \"Programación de nuevas tecnologías 2\",\n         \"eventoTipoDescripcion\": \"Trabajo Práctico\",\n         \"idInstitucion\": 1,\n         \"idCarrera\": 1\n     }\n   ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"[]\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/evento.controller.js",
    "groupTitle": "evento"
  },
  {
    "type": "get",
    "url": "/eventos/materia/:id",
    "title": "4. findByIdMateria : Busca y devuelve un evento, a partir del ID de una materia",
    "name": "findByIdMateria",
    "group": "evento",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n   {\n       \"idEvento\": 7,\n       \"idMateria\": 8,\n       \"idEventoTipo\": 1,\n       \"fecha\": \"10/07/2019\",\n       \"eventoDescripcion\": \"Asdasdasdasd.\",\n       \"materiaDescripcion\": \"Programación 1\",\n       \"eventoTipoDescripcion\": \"Trabajo Práctico\"\n   }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"[]\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/evento.controller.js",
    "groupTitle": "evento"
  },
  {
    "type": "put",
    "url": "/eventos/:id",
    "title": "5. insert : Se carga un nuevo evento, a partir de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "insert",
    "group": "evento",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    {\n     \"idEvento\": 2,\n     \"idMateria\": 11,\n     \"idEventoTipo\": 2,\n     \"fecha\": \"24/06/2036\",\n     \"descripcion\": \"masdasdjba\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"idMateria\" fails because [\"idMateria\" is required]\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/evento.controller.js",
    "groupTitle": "evento"
  },
  {
    "type": "delete",
    "url": "/eventos/:id",
    "title": "6. remove : Se quita Evento de la base",
    "name": "remove",
    "group": "evento",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Evento not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/evento.controller.js",
    "groupTitle": "evento"
  },
  {
    "type": "put",
    "url": "/eventos/:id",
    "title": "7. update : Se carga un nuevo evento, a parti de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "update",
    "group": "evento",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 NO CONTENT",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"idMateria\" fails because [\"idMateria\" is required]\"\n}\n\nHTTP/1.1 404 Not Found\n{\n  \"error\": \"Evento not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/evento.controller.js",
    "groupTitle": "evento"
  },
  {
    "type": "get",
    "url": "/institucion/",
    "title": "1. findAll : Devuelve todos las instituciones.",
    "name": "findAll",
    "group": "institucion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n [\n   {\n       \"idInstitucion\": 1,\n       \"descripcion\": \"ORT\",\n       \"idAlumnx\": 1\n   },\n   {\n       \"idInstitucion\": 2,\n       \"descripcion\": \"UBA\",\n       \"idAlumnx\": 2\n   },\n   {\n       \"idInstitucion\": 3,\n       \"descripcion\": \"UTN\",\n       \"idAlumnx\": 5\n   },\n   {\n       \"idInstitucion\": 4,\n       \"descripcion\": \"IUHI\",\n       \"idAlumnx\": 6\n   },\n   {\n       \"idInstitucion\": 5,\n       \"descripcion\": \"USAL\",\n       \"idAlumnx\": 6\n   },\n   {\n       \"idInstitucion\": 7,\n       \"descripcion\": \"ITBA Belgrano\",\n       \"idAlumnx\": 1\n   }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/institucion.controller.js",
    "groupTitle": "institucion"
  },
  {
    "type": "get",
    "url": "/institucion/:id",
    "title": "2. findById : Busca y devuelve una institucion, a partir de un ID recibido.",
    "name": "findById",
    "group": "institucion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  {\n     \"idInstitucion\": 3,\n     \"descripcion\": \"UTN\",\n     \"idAlumnx\": 5\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Institucion not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/institucion.controller.js",
    "groupTitle": "institucion"
  },
  {
    "type": "get",
    "url": "/institucion/alumnx/:id",
    "title": "3. findByIdAlumnx : Busca y devuelve una institucion, a partir del ID de un alumnx",
    "name": "findByIdAlumnx",
    "group": "institucion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n   {\n       \"idInstitucion\": 1,\n       \"descripcion\": \"ORT\",\n       \"idAlumnx\": 1\n   },\n   {\n       \"idInstitucion\": 7,\n       \"descripcion\": \"ITBA Belgrano\",\n       \"idAlumnx\": 1\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"[]\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/institucion.controller.js",
    "groupTitle": "institucion"
  },
  {
    "type": "put",
    "url": "/institucion/:id",
    "title": "4. insert : Se carga una nueva institucion, a partir de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "insert",
    "group": "institucion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    {\n         \"idInstitucion\": 1,\n         \"descripcion\": \"UTN\",\n         \"idAlumnx\": 3\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"idAlumnx\" fails because [\"idAlumnx\" is required]\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/institucion.controller.js",
    "groupTitle": "institucion"
  },
  {
    "type": "delete",
    "url": "/institucion/:id",
    "title": "5. remove : Se quita institucion de la base",
    "name": "remove",
    "group": "institucion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"mensaje\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Institucions not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/institucion.controller.js",
    "groupTitle": "institucion"
  },
  {
    "type": "put",
    "url": "/institucion/:id",
    "title": "6. update : Se carga una nueva institucion, a parti de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "update",
    "group": "institucion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\nHTTP/1.1 204 NO CONTENT",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"idAlumnx\" fails because [\"idAlumnx\" is required]\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/institucion.controller.js",
    "groupTitle": "institucion"
  },
  {
    "type": "get",
    "url": "/materia/",
    "title": "1. findAll : Devuelve todas las materias.",
    "name": "findAll",
    "group": "materia",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n     {\n         \"idMateria\": 8,\n         \"idCarrera\": 1,\n         \"descripcion\": \"Programación 1\"\n     },\n     {\n         \"idMateria\": 9,\n         \"idCarrera\": 1,\n         \"descripcion\": \"Taller de programación 1\"\n     },\n     {\n         \"idMateria\": 10,\n         \"idCarrera\": 1,\n         \"descripcion\": \"BD2\"\n     },\n     {\n         \"idMateria\": 11,\n         \"idCarrera\": 1,\n         \"descripcion\": \"Programación de nuevas tecnologías 2\"\n     },\n     {\n         \"idMateria\": 12,\n         \"idCarrera\": 2,\n         \"descripcion\": \"Sociedad y estado\"\n     },\n     {\n         \"idMateria\": 13,\n         \"idCarrera\": 3,\n         \"descripcion\": \"Física 1\"\n     },\n     {\n         \"idMateria\": 14,\n         \"idCarrera\": 4,\n         \"descripcion\": \"Anatomía\"\n     },\n     {\n         \"idMateria\": 15,\n         \"idCarrera\": 5,\n         \"descripcion\": \"Ética Profesional\"\n     },\n     {\n         \"idMateria\": 17,\n         \"idCarrera\": 1,\n         \"descripcion\": \"Comunicaciones Efectivas I\"\n     }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/materia.controller.js",
    "groupTitle": "materia"
  },
  {
    "type": "get",
    "url": "/materia/:id",
    "title": "2. findById : Busca y devuelve una materia, a partir de un ID recibido.",
    "name": "findById",
    "group": "materia",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n {\n   \"idMateria\": 14,\n   \"idCarrera\": 4,\n   \"descripcion\": \"Anatomía\"\n }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Materias not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/materia.controller.js",
    "groupTitle": "materia"
  },
  {
    "type": "get",
    "url": "/materia/alumnx/:id",
    "title": "3. findByIdAlumnx : Busca y devuelve una materia, a partir del ID de un alumnx",
    "name": "findByIdAlumnx",
    "group": "materia",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n     {\n         \"idMateria\": 10,\n         \"idCarrera\": 1,\n         \"descripcion\": \"BD2\",\n         \"idAlumnx\": 1\n     },\n     {\n         \"idMateria\": 17,\n         \"idCarrera\": 1,\n         \"descripcion\": \"Comunicaciones Efectivas I\",\n         \"idAlumnx\": 1\n     },\n     {\n         \"idMateria\": 8,\n         \"idCarrera\": 1,\n         \"descripcion\": \"Programación 1\",\n         \"idAlumnx\": 1\n     },\n     {\n         \"idMateria\": 11,\n         \"idCarrera\": 1,\n         \"descripcion\": \"Programación de nuevas tecnologías 2\",\n         \"idAlumnx\": 1\n     },\n     {\n         \"idMateria\": 9,\n         \"idCarrera\": 1,\n         \"descripcion\": \"Taller de programación 1\",\n         \"idAlumnx\": 1\n     }\n ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Materias not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/materia.controller.js",
    "groupTitle": "materia"
  },
  {
    "type": "get",
    "url": "/materia/carrera/:id",
    "title": "4. findByIdInstitucion : Busca y devuelve una materia, a partir del ID de una carrera",
    "name": "findByIdInstitucion",
    "group": "materia",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n     \"idMateria\": 12,\n     \"idCarrera\": 2,\n     \"descripcion\": \"Sociedad y estado\"\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Materias not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/materia.controller.js",
    "groupTitle": "materia"
  },
  {
    "type": "put",
    "url": "/materia/:id",
    "title": "5. insert : Se carga una nueva materia, a partir de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "insert",
    "group": "materia",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    {\n       \"idMateria\": 10,\n       \"idCarrera\": 1,\n       \"descripcion\": \"LENGUA\",\n       \"idAlumnx\": 1\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"idCarrera\" fails because [\"idCarrera\" is required]\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/materia.controller.js",
    "groupTitle": "materia"
  },
  {
    "type": "delete",
    "url": "/materia/:id",
    "title": "6. remove : Se quita materia de la base",
    "name": "remove",
    "group": "materia",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"mensaje\": \"[]\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Materia not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/materia.controller.js",
    "groupTitle": "materia"
  },
  {
    "type": "put",
    "url": "/materia/:id",
    "title": "7. update : Se carga una nueva materia, a parti de los datos enviados. Se verifica que esten todos los datos cargados.",
    "name": "update",
    "group": "materia",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\nHTTP/1.1 204 NO CONTENT",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"ValidationError: child \"idCarrera\" fails because [\"idCarrera\" is required]\"\n}\n\nHTTP/1.1 404 Not Found\n{\n  \"error\": \"Materia not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/materia.controller.js",
    "groupTitle": "materia"
  },
  {
    "type": "get",
    "url": "/pais/",
    "title": "1. findAll : Devuelve la lista de todos los Paises en la base de datos",
    "name": "findAll",
    "group": "pais",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n      {\n          \"idPais\": 1,\n          \"descripcion\": \"Argentina\"\n      },\n      {\n          \"idPais\": 2,\n          \"descripcion\": \"Uruguay\"\n      },\n      {\n          \"idPais\": 3,\n          \"descripcion\": \"Chile\"\n      },\n      {\n          \"idPais\": 4,\n          \"descripcion\": \"Venezuela\"\n      },\n      {\n          \"idPais\": 5,\n          \"descripcion\": \"Brasil\"\n      }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/pais.controller.js",
    "groupTitle": "pais"
  },
  {
    "type": "get",
    "url": "/pais/:id",
    "title": "2. findById : Busca y devuelve un pais, a partir de un ID recibido.",
    "name": "findById",
    "group": "pais",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n  {\n     \"idPais\": 3,\n     \"descripcion\": \"Chile\"\n\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"ERROR\": \"Pais not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/pais.controller.js",
    "groupTitle": "pais"
  }
] });
