/*
CREATE TABLE alumnx (
    id_alumnx INT PRIMARY KEY NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    apellido VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    id_pais INT NOT NULL,
    fecha_nacimiento DATE NOT NULL
);

INSERT INTO alumnx (nombre, apellido, email, password, id_pais, fecha_nacimiento)
    VALUES ('Juan', 'Perez', 'email@gmail.com', '12345', 54, '1990-01-01'),
           ('John', 'Doe', 'email2@gmail.com', '12345', 54, '1990-12-31'),
           ('Cosme', 'Fulanito', 'cosmefulanito@gmail.com','123456', 54, '1990-12-31'),
           ('Alfredo', 'Gelberg', 'alfred@hotmail.com','123456', 54, '1990-12-31'),
           ('Mariano', 'Petruso', 'mpetruso@gmail.com','123456', 54, '1990-12-31'),
           ('Cristian', 'Berlingo', 'berlingo@yahoo.com.ar','123456', 54, '1990-12-31'),
           ('Barbara', 'Barni', 'bb@gmail.com','123456', 54, '1990-12-31'),
           ('Martin', 'Suarez', 'suarez@gmail.uy','123456', 54, '1990-12-31'),
           ('Juan Jose', 'Jimene', 'jjjdiegol@gmail.com','123456', 54, '1990-12-31'),
           ('Esteban', 'Tassi', 'tassiesteban@hotmail.com','123456', 54, '1990-12-31'),
           ('Ludmila', 'Negri', 'negriludmila@gmail.com','123456', 54, '1990-12-31'),
           ('Nicky', 'Xipola', 'nicky@gmail.com','123456', 54, '1990-12-31');

CREATE TABLE evento (
    id_evento INT PRIMARY KEY NOT NULL,
    id_agenda INT NOT NULL,
    tipo_evento INT NOT NULL,
    fecha DATE NOT NULL,
    descripcion VARCHAR(140) NOT NULL
);*/

INSERT INTO institucion (id_alumnx, descripcion) values (1, "ORT"), (2, "UBA"), (5, "UTN"), (6, "IUHI"), (6, "USAL");
INSERT INTO carrera (id_institucion, descripcion) values (1, "Analista de sistemas"), (2, "Abogacía"), (3, "Ingeniería electrónica"), (4, "Lic. en Enfermería"), (5, "Medicina");
INSERT INTO materia (id_carrera, descripcion) values (1, "Programación 1"), (1, "Taller de programación 1"), (1, "BD2"), (1, "Programación de nuevas tecnologías 2"), (2, "Sociedad y estado"), (3, "Física 1"), (4, "Anatomía"), (5, "Ética Profesional");
INSERT INTO agenda (id_materia) select id_materia from materia;
INSERT INTO evento_tipo (descripcion) values ("Trabajo Práctico"), ("Parcial"), ("Final");
INSERT INTO evento (id_agenda, id_evento_tipo, fecha, descripcion) values (11, 1, "2019-06-18", "quasar y vue"), (11, 2, "2019-06-24", "normalizacion");

SELECT * FROM evento e 
inner join agenda a on e.id_agenda = a.id_agenda
inner join materia m on a.id_materia = m.id_materia
inner join carrera c on m.id_carrera = c.id_carrera
inner join institucion i on c.id_institucion = i.id_institucion
where i.id_alumnx = 1;

select * from `evento` 
inner join `agenda` on `evento`.`id_evento` = 
    `agenda`.`id_agenda` inner join `materia` on `agenda`.`id_agenda` = 
    `materia`.`id_materia` inner join `carrera` on `materia`.`id_materia` = 
    `carrera`.`id_carrera` inner join `institucion` on 
    `carrera`.`id_carrera` = `institucion`.`id_institucion` where 
    `institucion`.`id_alumnx` = 1









