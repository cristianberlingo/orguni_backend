CREATE DATABASE  IF NOT EXISTS `orguni` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `orguni`;
-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: orguni
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumnx`
--

DROP TABLE IF EXISTS `alumnx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumnx` (
  `id_alumnx` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  PRIMARY KEY (`id_alumnx`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumnx`
--

LOCK TABLES `alumnx` WRITE;
/*!40000 ALTER TABLE `alumnx` DISABLE KEYS */;
INSERT INTO `alumnx` VALUES (1,'Juan','Perez','email@gmail.com','12341234',598,'2019-06-19'),(2,'John','Doe','email2@gmail.com','12345',54,'1990-12-31'),(3,'Juan','Perez','email@gmail.com','12345',54,'1990-01-01'),(4,'John','Doe','email2@gmail.com','12345',54,'1990-12-31'),(5,'Cosme','Fulanito','cosmefulanito@gmail.com','123456',54,'1990-12-31'),(6,'Alfredo','Gelberg','alfred@hotmail.com','123456',54,'1990-12-31'),(7,'Alfredo','Barni','barbaraGomez@gmail.com','asd123456',54,'1992-02-03'),(8,'Cristian','Berlingo','berlingo@yahoo.com.ar','123456',54,'1990-12-31'),(9,'Barbara','Barni','bb@gmail.com','123456',54,'1990-12-31'),(10,'Homero','Thompson','homerothompson@gmail.com','asd456',598,'1994-06-02'),(17,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(19,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(20,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(21,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(22,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(23,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28');
/*!40000 ALTER TABLE `alumnx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institucion`
--

DROP TABLE IF EXISTS `institucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institucion` (
  `id_institucion` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  `id_alumnx` int(11) NOT NULL,
  PRIMARY KEY (`id_institucion`),
  KEY `fk_institucion_1_idx` (`id_alumnx`),
  CONSTRAINT `fk_institucion_1` FOREIGN KEY (`id_alumnx`) REFERENCES `alumnx` (`id_alumnx`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institucion`
--

LOCK TABLES `institucion` WRITE;
/*!40000 ALTER TABLE `institucion` DISABLE KEYS */;
INSERT INTO `institucion` VALUES (1,'ORT',1),(2,'UBA',2),(3,'UTN',5),(4,'IUHI',6),(5,'USAL',6),(7,'ITBA Belgrano',1);
/*!40000 ALTER TABLE `institucion` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `carrera`
--

DROP TABLE IF EXISTS `carrera`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carrera` (
  `id_carrera` int(11) NOT NULL AUTO_INCREMENT,
  `id_institucion` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id_carrera`),
  KEY `fk_carrera_1_idx` (`id_institucion`),
  CONSTRAINT `fk_carrera_1` FOREIGN KEY (`id_institucion`) REFERENCES `institucion` (`id_institucion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carrera`
--

LOCK TABLES `carrera` WRITE;
/*!40000 ALTER TABLE `carrera` DISABLE KEYS */;
INSERT INTO `carrera` VALUES (1,1,'Analista de sistemas'),(2,2,'Abogacía'),(3,3,'Ingeniería electrónica'),(4,4,'Lic. en Enfermería'),(5,5,'Medicina'),(7,1,'Contabilidad');
/*!40000 ALTER TABLE `carrera` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materia`
--

DROP TABLE IF EXISTS `materia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materia` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `id_carrera` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id_materia`),
  KEY `fk_materia_1_idx` (`id_carrera`),
  CONSTRAINT `fk_materia_1` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materia`
--

LOCK TABLES `materia` WRITE;
/*!40000 ALTER TABLE `materia` DISABLE KEYS */;
INSERT INTO `materia` VALUES (8,1,'Programación 1'),(9,1,'Taller de programación 1'),(10,1,'BD2'),(11,1,'Programación de nuevas tecnologías 2'),(12,2,'Sociedad y estado'),(13,3,'Física 1'),(14,4,'Anatomía'),(15,5,'Ética Profesional'),(17,1,'Comunicaciones Efectivas I');
/*!40000 ALTER TABLE `materia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


--
-- Table structure for table `evento_tipo`
--

DROP TABLE IF EXISTS `evento_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_tipo` (
  `id_evento_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id_evento_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_tipo`
--

LOCK TABLES `evento_tipo` WRITE;
/*!40000 ALTER TABLE `evento_tipo` DISABLE KEYS */;
INSERT INTO `evento_tipo` VALUES (1,'Trabajo Práctico'),(2,'Parcial'),(3,'Final');
/*!40000 ALTER TABLE `evento_tipo` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `id_evento` int(11) NOT NULL AUTO_INCREMENT,
  `id_materia` int(11) NOT NULL,
  `id_evento_tipo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` varchar(140) NOT NULL,
  PRIMARY KEY (`id_evento`),
  KEY `fk_evento_2_idx` (`id_evento_tipo`),
  KEY `fk_evento_1_idx` (`id_materia`),
  CONSTRAINT `fk_evento_1` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_evento_2` FOREIGN KEY (`id_evento_tipo`) REFERENCES `evento_tipo` (`id_evento_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` VALUES (1,11,1,'2019-06-18','quasar y vue'),(2,11,2,'2019-06-24','normalizacion'),(3,9,3,'2019-07-01','lorem ipsum dolor sit amet'),(7,8,1,'2019-07-10','Asdasdasdasd.'),(8,8,1,'2019-07-10','Asdasdasdasd.');
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;



DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'Argentina'),(2,'Uruguay'),(3,'Chile'),(4,'Venezuela'),(5,'Brasil');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-24 23:44:58
