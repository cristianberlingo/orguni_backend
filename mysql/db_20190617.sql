CREATE DATABASE  IF NOT EXISTS `orguni` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `orguni`;
-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: orguni
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumnx`
--

DROP TABLE IF EXISTS `alumnx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumnx` (
  `id_alumnx` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  PRIMARY KEY (`id_alumnx`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumnx`
--

LOCK TABLES `alumnx` WRITE;
/*!40000 ALTER TABLE `alumnx` DISABLE KEYS */;
INSERT INTO `alumnx` VALUES (1,'Juan','Perez','email@gmail.com','12345',54,'1990-01-01'),(2,'John','Doe','email2@gmail.com','12345',54,'1990-12-31'),(3,'Juan','Perez','email@gmail.com','12345',54,'1990-01-01'),(4,'John','Doe','email2@gmail.com','12345',54,'1990-12-31'),(5,'Cosme','Fulanito','cosmefulanito@gmail.com','123456',54,'1990-12-31'),(6,'Alfredo','Gelberg','alfred@hotmail.com','123456',54,'1990-12-31'),(7,'Alfredo','Barni','barbaraGomez@gmail.com','asd123456',54,'1992-02-03'),(8,'Cristian','Berlingo','berlingo@yahoo.com.ar','123456',54,'1990-12-31'),(9,'Barbara','Barni','bb@gmail.com','123456',54,'1990-12-31'),(10,'Homero','Thompson','homerothompson@gmail.com','asd456',598,'1994-06-02'),(17,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(19,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(20,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(21,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(22,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28'),(23,'Julieta','Funseca','Funseca003@gmail.com','123456',54,'1989-12-28');
/*!40000 ALTER TABLE `alumnx` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-17 13:03:53
