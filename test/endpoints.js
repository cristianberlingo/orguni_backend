const endpoints = {
    alumnx: 'alumnx',
    institucion: 'institucion',
    carrera: 'carrera',
    materia: 'materia',
    evento: 'evento',
    eventoTipo: 'eventoTipo',
    pais: 'pais'
}

module.exports = {
    endpoints
}