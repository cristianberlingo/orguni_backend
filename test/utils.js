const _ = require('lodash')
const fs = require('fs');
const tempFile = "./test/tempData.json";

const testFields = (body, fields, entityName) => {
    if (_.isEmpty(body)) {
        throw "No se encontró " + entityName
    }
    for (const field of fields) {
        if (!body.hasOwnProperty(field)) {
            throw "No tiene " + field
        }
    }
}

const testArray = (body, fields, entityName) => {
    if (!_.isArray(body)) {
        throw "La respuesta no es una lista"
    }
    if (_.isEmpty(body)) {
        throw "No se encontraron " + entityName + "'s"
    }
    for (const item of body) {
        testFields(item, fields, entityName)
    }
}

const retorno = (err) => {
    if (err.hasOwnProperty('message')) {
        return err.message
    }
    return err
}

const readTempData = () => {
    return JSON.parse(fs.readFileSync(tempFile, 'utf-8'))
}

const addTempData = (key, value) => {
    const data = readTempData()
    data[key] = value
    fs.writeFileSync(tempFile, JSON.stringify(data));
}

const cleanTempData = () => {
    fs.writeFileSync(tempFile, JSON.stringify({}));
}

const evalTest = (resultTest) => {
    contadores.total++;
    if (resultTest === true) {
        console.info("OK");
        contadores.ok++;
    } else {
        contadores.fail++;
        console.error(resultTest);
    }

}

const ucfirst = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const contadores = { total: 0, ok: 0, fail: 0 };

module.exports = {
    testFields,
    testArray,
    retorno,
    readTempData,
    addTempData,
    cleanTempData,
    evalTest,
    contadores,
    ucfirst
}