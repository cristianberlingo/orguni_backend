const utils = require('./utils')

const serverUrl = 'http://127.0.0.1:3000/'

const fields = require('./fields')

const jsons = require('./jsons')

const findAll = require('./unitTests/findAllTest')
const findById = require('./unitTests/findByIdTest')
const findByEntityId = require('./unitTests/findByEntityIdTest')
const insert = require('./unitTests/insertTest')
const update = require('./unitTests/updateTest')
const remove = require('./unitTests/removeTest')

const mailTest = require('./unitTests/mailTest')
const alumnxUploadTest = require('./unitTests/alumnxUploadTest')

const { endpoints } = require('./endpoints')

const { idNames } = require('./idNames')

async function main(noFelicesEnabled = false) {
    utils.cleanTempData()
    console.log("Ejecutando tests, aguarde...");

    utils.evalTest(await mailTest('smolina.sbs@gmail.com'))

    utils.evalTest(await findAll(serverUrl, fields.fieldsAlumnx, endpoints.alumnx))
    if (noFelicesEnabled)
        utils.evalTest(await findAll(serverUrl, fields.fieldsAlumnxNF, endpoints.alumnx))

    utils.evalTest(await insert(serverUrl, fields.fieldsAlumnx, jsons.jsonInsertAlumnx, endpoints.alumnx))
    if (noFelicesEnabled)
        utils.evalTest(await insert(serverUrl, fields.fieldsAlumnx, jsons.jsonInsertAlumnxNF, endpoints.alumnx))

    utils.evalTest(await alumnxUploadTest(serverUrl))

    utils.evalTest(await findById(serverUrl, fields.fieldsAlumnx, endpoints.alumnx, utils.readTempData()[idNames.alumnx]))
    if (noFelicesEnabled)
        utils.evalTest(await findById(serverUrl, fields.fieldsAlumnx, endpoints.alumnx, 'as'))

    utils.evalTest(await update(serverUrl, jsons.jsonUpdateAlumnx, utils.readTempData()[idNames.alumnx], endpoints.alumnx))
    if (noFelicesEnabled)
        utils.evalTest(await update(serverUrl, jsons.jsonUpdateAlumnx, 'asd', endpoints.alumnx))

    utils.evalTest(await findAll(serverUrl, fields.fieldsInstitucion, endpoints.institucion))
    if (noFelicesEnabled)
        utils.evalTest(await findAll(serverUrl, fields.fieldsInstitucionNF, endpoints.institucion))

    utils.evalTest(await insert(serverUrl, fields.fieldsInstitucion, jsons.jsonInsertInstitucion, endpoints.institucion, idNames.alumnx, utils.readTempData()[idNames.alumnx]))
    if (noFelicesEnabled)
        utils.evalTest(await insert(serverUrl, fields.fieldsInstitucion, jsons.jsonInsertInstitucionNF, endpoints.institucion, idNames.alumnx, utils.readTempData()[idNames.alumnx]))

    utils.evalTest(await findById(serverUrl, fields.fieldsInstitucion, endpoints.institucion, utils.readTempData()[idNames.institucion]))
    if (noFelicesEnabled)
        utils.evalTest(await findById(serverUrl, fields.fieldsInstitucion, endpoints.institucion, 1234))

    utils.evalTest(await findByEntityId(serverUrl, fields.fieldsInstitucion, endpoints.institucion, endpoints.alumnx, utils.readTempData()[idNames.alumnx]))
    if (noFelicesEnabled)
        utils.evalTest(await findByEntityId(serverUrl, fields.fieldsInstitucion, endpoints.institucion, endpoints.alumnx, 1234))

    utils.evalTest(await findAll(serverUrl, fields.fieldsCarrera, endpoints.carrera))
    if (noFelicesEnabled)
        utils.evalTest(await findAll(serverUrl, fields.fieldsCarreraNF, endpoints.carrera))

    utils.evalTest(await insert(serverUrl, fields.fieldsCarrera, jsons.jsonInsertCarrera, endpoints.carrera, idNames.institucion, utils.readTempData()[idNames.institucion]))
    if (noFelicesEnabled)
        utils.evalTest(await insert(serverUrl, fields.fieldsCarrera, jsons.jsonInsertCarrera, endpoints.carrera, idNames.institucion, 12345))

    utils.evalTest(await findById(serverUrl, fields.fieldsCarrera, endpoints.carrera, utils.readTempData()[idNames.carrera]))
    if (noFelicesEnabled)
        utils.evalTest(await findById(serverUrl, fields.fieldsCarrera, endpoints.carrera, 'qwer'))

    utils.evalTest(await findByEntityId(serverUrl, fields.fieldsCarreraByAlumnx, endpoints.carrera, endpoints.alumnx, utils.readTempData()[idNames.alumnx]))
    if (noFelicesEnabled)
        utils.evalTest(await findByEntityId(serverUrl, fields.fieldsCarreraByAlumnx, endpoints.carrera, endpoints.alumnx, 1234))

    if (noFelicesEnabled)
        utils.evalTest(await findByEntityId(serverUrl, fields.fieldsCarrera, endpoints.carrera, endpoints.institucion, 1234))
    utils.evalTest(await findByEntityId(serverUrl, fields.fieldsCarrera, endpoints.carrera, endpoints.institucion, utils.readTempData()[idNames.institucion]))


    utils.evalTest(await findAll(serverUrl, fields.fieldsMateria, endpoints.materia))
    if (noFelicesEnabled)
        utils.evalTest(await findAll(serverUrl, fields.fieldsMateriaNF, endpoints.materia))

    utils.evalTest(await insert(serverUrl, fields.fieldsMateria, jsons.jsonInsertMateria, endpoints.materia, idNames.carrera, utils.readTempData()[idNames.carrera]))
    if (noFelicesEnabled)
        utils.evalTest(await insert(serverUrl, fields.fieldsMateria, jsons.jsonInsertMateria, endpoints.materia, 'qwer', utils.readTempData()[idNames.carrera]))

    utils.evalTest(await findById(serverUrl, fields.fieldsMateria, endpoints.materia, utils.readTempData()[idNames.materia]))
    if (noFelicesEnabled)
        utils.evalTest(await findById(serverUrl, fields.fieldsMateria, endpoints.materia, 12345))

    utils.evalTest(await findByEntityId(serverUrl, fields.fieldsMateria, endpoints.materia, endpoints.alumnx, utils.readTempData()[idNames.alumnx]))
    if (noFelicesEnabled)
        utils.evalTest(await findByEntityId(serverUrl, fields.fieldsMateria, endpoints.materia, endpoints.alumnx, 1234))

    utils.evalTest(await findByEntityId(serverUrl, fields.fieldsMateria, endpoints.materia, endpoints.carrera, utils.readTempData()[idNames.carrera]))
    if (noFelicesEnabled)
        utils.evalTest(await findByEntityId(serverUrl, fields.fieldsMateria, endpoints.materia, endpoints.carrera, 321654))

    utils.evalTest(await findAll(serverUrl, fields.fieldsEvento, endpoints.evento))
    if (noFelicesEnabled)
        utils.evalTest(await findAll(serverUrl, fields.fieldsEventoNF, endpoints.evento))

    utils.evalTest(await insert(serverUrl, fields.fieldsEvento, jsons.jsonInsertEvento, endpoints.evento, idNames.materia, utils.readTempData()[idNames.materia]))
    if (noFelicesEnabled)
        utils.evalTest(await insert(serverUrl, fields.fieldsEvento, jsons.jsonInsertEvento, endpoints.evento, idNames.materia, 1234))

    utils.evalTest(await findById(serverUrl, fields.fieldsEvento, endpoints.evento, utils.readTempData()[idNames.evento]))
    if (noFelicesEnabled)
        utils.evalTest(await findById(serverUrl, fields.fieldsEvento, endpoints.evento, 1234))

    utils.evalTest(await findByEntityId(serverUrl, fields.fieldsEventoByIdAlumnx, endpoints.evento, endpoints.alumnx, utils.readTempData()[idNames.alumnx]))
    if (noFelicesEnabled)
        utils.evalTest(await findByEntityId(serverUrl, fields.fieldsEventoByIdAlumnx, endpoints.evento, endpoints.alumnx, 1234))

    utils.evalTest(await findByEntityId(serverUrl, fields.fieldsEventoByIdMateria, endpoints.evento, endpoints.materia, utils.readTempData()[idNames.materia]))
    if (noFelicesEnabled)
        utils.evalTest(await findByEntityId(serverUrl, fields.fieldsEventoByIdMateria, endpoints.evento, endpoints.materia, 1234))

    utils.evalTest(await update(serverUrl, jsons.jsonUpdateEvento, utils.readTempData()[idNames.evento], endpoints.evento))
    if (noFelicesEnabled)
        utils.evalTest(await update(serverUrl, jsons.jsonUpdateEvento, utils.readTempData()[idNames.evento], 1234))

    utils.evalTest(await update(serverUrl, jsons.jsonUpdateMateria, utils.readTempData()[idNames.materia], endpoints.materia))
    if (noFelicesEnabled)
        utils.evalTest(await update(serverUrl, jsons.jsonUpdateMateria, 1234, endpoints.materia))

    utils.evalTest(await update(serverUrl, jsons.jsonUpdateInstitucion, utils.readTempData()[idNames.institucion], endpoints.institucion))
    if (noFelicesEnabled)
        utils.evalTest(await update(serverUrl, jsons.jsonUpdateInstitucion, 1234, endpoints.institucion))

    utils.evalTest(await update(serverUrl, jsons.jsonUpdateCarrera, utils.readTempData()[idNames.carrera], endpoints.carrera))
    if (noFelicesEnabled)
        utils.evalTest(await update(serverUrl, jsons.jsonUpdateCarrera, 1234, endpoints.carrera))

    utils.evalTest(await remove(serverUrl, endpoints.evento, utils.readTempData()[idNames.evento]))
    if (noFelicesEnabled)
        utils.evalTest(await remove(serverUrl, endpoints.evento, 1234))

    utils.evalTest(await remove(serverUrl, endpoints.materia, utils.readTempData()[idNames.materia]))
    if (noFelicesEnabled)
        utils.evalTest(await remove(serverUrl, endpoints.materia, 'qwer'))

    utils.evalTest(await remove(serverUrl, endpoints.carrera, utils.readTempData()[idNames.carrera]))
    if (noFelicesEnabled)
        utils.evalTest(await remove(serverUrl, endpoints.carrera, 'asdf'))

    utils.evalTest(await remove(serverUrl, endpoints.institucion, utils.readTempData()[idNames.institucion]))
    if (noFelicesEnabled)
        utils.evalTest(await remove(serverUrl, endpoints.institucion, 1234))

    utils.evalTest(await remove(serverUrl, endpoints.alumnx, utils.readTempData()[idNames.alumnx]))
    if (noFelicesEnabled)
        utils.evalTest(await remove(serverUrl, endpoints.alumnx, 1234))

    utils.evalTest(await findAll(serverUrl, fields.fieldsPais, endpoints.pais))
    if (noFelicesEnabled)
        utils.evalTest(await findAll(serverUrl, fields.fieldsPaisNF, endpoints.pais))

    utils.evalTest(await findById(serverUrl, fields.fieldsPais, endpoints.pais, 1))
    if (noFelicesEnabled)
        utils.evalTest(await findById(serverUrl, fields.fieldsPais, endpoints.pais, 11111))

    utils.evalTest(await findAll(serverUrl, fields.fieldsEventoTipo, endpoints.eventoTipo))
    if (noFelicesEnabled)
        utils.evalTest(await findAll(serverUrl, fields.fieldsEventoTipoNF, endpoints.eventoTipo))

    utils.evalTest(await findById(serverUrl, fields.fieldsEventoTipo, endpoints.eventoTipo, 1))
    if (noFelicesEnabled)
        utils.evalTest(await findById(serverUrl, fields.fieldsEventoTipo, endpoints.eventoTipo, 1234))


    console.log('-----------------------------------------------------');
    console.log("Tests TOTAL: " + utils.contadores.total);
    console.log("Tests OK: " + utils.contadores.ok + "/" + utils.contadores.total);
    console.log("Tests FAIL: " + utils.contadores.fail + "/" + utils.contadores.total);
}

main(false)