const request = require('request-promise-native')
const utils = require('../utils')

module.exports = async function findByEntityIdTest(serverUrl, fields, endpoint, entity, targetId) {

    const options = {
        uri: serverUrl + endpoint + '/' + entity + '/' + targetId,
        json: true
    }

    try {
        const body = await request(options)
        utils.testArray(body, fields, endpoint + ' by ' + entity)
        return true
    } catch (err) {
        return utils.retorno(err)
    }
}