const request = require('request-promise-native')
const utils = require('../utils')

module.exports = async function removeTest(serverUrl, endpoint, targetId) {

    const options = {
        method: 'DELETE',
        uri: serverUrl + endpoint + '/' + targetId,
        json: true
    }

    try {
        await request(options)
        return true
    } catch (err) {
        return utils.retorno(err)
    }

}