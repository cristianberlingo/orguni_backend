const request = require('request-promise-native');
const utils = require('../utils')

module.exports = async function updateTest(serverUrl, json, targetId, endpoint) {

    const options = {
        method: 'PUT',
        uri: serverUrl + endpoint + '/' + targetId,
        body: json,
        json: true
    }

    try {
        await request(options)
        return true
    } catch (err) {
        return utils.retorno(err)
    }
}
