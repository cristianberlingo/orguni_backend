const request = require('request-promise-native');
const utils = require('../utils')

module.exports = async function findAllTest(serverUrl, fields, endpoint) {

    const options = {
        uri: serverUrl + endpoint,
        json: true
    }

    try {
        const body = await request(options)
        utils.testArray(body, fields, endpoint)
        return true
    } catch (err) {
        return utils.retorno(err)
    }
}
