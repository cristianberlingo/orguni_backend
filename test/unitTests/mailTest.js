const mail = require('../../src/services/mail.service');
const mensaje = require('../../src/services/mensaje.service');
const utils = require('../utils')

module.exports = async function mailTest(destino) {

    try {
        const asunto = "Mail de prueba"
        const mensaje = "TEST"
        await mail.sendEmail(destino, asunto, mensaje);
        return true
    } catch (err) {
        utils.retorno(err)
    }

}


