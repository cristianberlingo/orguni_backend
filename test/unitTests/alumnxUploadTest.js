const fs = require("fs");
const request = require("request-promise-native");
const pathArchivo = __dirname + '/../../uploads/alumnx/avatars/1.jpg';
const utils = require('../utils')

module.exports = async function updateAlumnxTest(serverUrl) {
    const targetId = utils.readTempData().idAlumnx
    const options = {
        method: 'POST',
        uri: serverUrl + 'alumnx/uploadAvatar',
        formData: {
            alumnx_id: targetId,
            avatar: {
                value: fs.createReadStream(pathArchivo),
                options: {
                    filename: '1.jpg',
                    contentType: 'image/jpg'
                }
            }
        },
        headers: {
        }
    };
    try {
        await request(options)
        return true
    } catch (error) {
        return utils.retorno(error)
    }

}