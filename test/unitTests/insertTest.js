const request = require('request-promise-native');
const utils = require('../utils')

module.exports = async function insertTest(serverUrl, fields, json, endpoint, idIntegridadName = null, idIntegridadValue = null) {
    if (idIntegridadName !== null && idIntegridadValue !== null) {
        json[idIntegridadName] = idIntegridadValue
    }
    const idName = 'id' + utils.ucfirst(endpoint)
    const options = {
        method: 'POST',
        uri: serverUrl + endpoint,
        body: json,
        json: true
    }
    try {
        const body = await request(options)
        utils.testFields(body, fields, endpoint)
        utils.addTempData(idName, body[idName])
        return true
    } catch (err) {
        return utils.retorno(err)
    }
}
