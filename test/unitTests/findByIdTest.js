const request = require('request-promise-native')
const utils = require('../utils')

module.exports = async function findByIdTest(serverUrl, fields, endpoint, targetId) {
    const options = {
        uri: serverUrl + endpoint + '/' + targetId,
        json: true
    }

    try {
        const body = await request(options)
        utils.testFields(body, fields, endpoint)
        return true
    } catch (err) {
        return utils.retorno(err)
    }
}