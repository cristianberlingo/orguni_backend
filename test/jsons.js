const jsonInsertAlumnx = {
    idAlumnx: 4,
    nombre: "Julieta",
    apellido: "Funseca",
    email: "Funseca003@gmail.com",
    password: "123456",
    fechaNacimiento: "28/12/1989",
    idPais: 54
}
const jsonInsertAlumnxNF = {
    idAlumnx: 4,
    nombre: "Julieta",
    apellido: "Funseca",
    email: "Funseca003@gmail.com",
    password: "123456",
    fechaNacimiento: "28/12/89",
    idPais: 54
}
const jsonUpdateAlumnx = {
    nombre: "Alfredo",
    apellido: "Barni",
    email: "gomez@gmail.com",
    password: "asd123456",
    fechaNacimiento: "03/02/1992",
    idPais: 54
}
const jsonUpdateAlumnxNF = {
    nombre: "Alfredo",
    apellido: "Barni",
    email: "gomez@gmail.com1234",
    password: "asd123456",
    fechaNacimiento: "03/02/1992",
    idPais: 54
}
const jsonInsertInstitucion = {
    idAlumnx: 1,
    descripcion: "UAI"
}
const jsonInsertInstitucionNF = {
    idAlumnx: "1a",
    descripcion: "UAI"
}
const jsonUpdateInstitucion = {
    idInstitucion: 1,
    idAlumnx: 1,
    descripcion: "UADE"
}
const jsonInsertCarrera = {
    idInstitucion: 1,
    descripcion: "Bioingeniería"
}
const jsonUpdateCarrera = {
    idCarrera: 1,
    idInstitucion: 1,
    descripcion: "ingeniería electrónica"
}
const jsonInsertMateria = {
    idCarrera: 1,
    descripcion: "Matemática I"
}
const jsonUpdateMateria = {
    idMateria: 1,
    idCarrera: 1,
    descripcion: "Matemática I"
}
const jsonInsertEvento = {
    idEvento: 10,
    idMateria: 8,
    idEventoTipo: 1,
    fecha: "10/07/2019",
    descripcion: "Asdasdasdasd."
}
const jsonUpdateEvento = {
    idEvento: 5,
    idMateria: 9,
    idEventoTipo: 1,
    fecha: "10/07/2019",
    descripcion: "Asdasdasdasd."
}

module.exports = {
    jsonInsertAlumnx,
    jsonInsertAlumnxNF,
    jsonInsertCarrera,
    jsonInsertEvento,
    jsonInsertInstitucion,
    jsonInsertInstitucionNF,
    jsonInsertMateria,
    jsonUpdateAlumnx,
    jsonUpdateAlumnxNF,
    jsonUpdateCarrera,
    jsonUpdateEvento,
    jsonUpdateInstitucion,
    jsonUpdateMateria
}