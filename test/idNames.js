const idNames = {
    alumnx: 'idAlumnx',
    institucion: 'idInstitucion',
    carrera: 'idCarrera',
    materia: 'idMateria',
    evento: 'idEvento',
    eventoTipo: 'idEventoTipo',
    pais: 'idPais'
}

module.exports = {
    idNames
}