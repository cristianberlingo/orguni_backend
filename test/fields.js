const fieldsEvento = ['idEvento', 'idMateria', 'idEventoTipo', 'descripcion', 'fecha']
const fieldsEventoNF = ['idEvento', 'idMateria', 'idEventoTipo', 'descripcion', 'fecha', 'nota']
const fieldsEventoByIdAlumnx = ['idEvento', 'idMateria', 'idEventoTipo', 'eventoDescripcion', 'fecha', 'institucionDescripcion', 'carreraDescripcion', 'materiaDescripcion', 'eventoTipoDescripcion', 'idInstitucion', 'idCarrera']
const fieldsEventoByIdMateria = ['idEvento', 'idMateria', 'idEventoTipo', 'eventoDescripcion', 'fecha', 'materiaDescripcion', 'eventoTipoDescripcion']
const fieldsInstitucion = ['idAlumnx', 'idInstitucion', 'descripcion']
const fieldsCarrera = ['idCarrera', 'idInstitucion', 'descripcion']
const fieldsCarreraNF = ['idCarrera', 'idInstitucion', 'descripcion', 'duracion']
const fieldsCarreraByAlumnx = ['idCarrera', 'idInstitucion', 'carreraDescripcion', 'institucionDescripcion', 'idAlumnx']
const fieldsMateria = ['idCarrera', 'idMateria', 'descripcion']
const fieldsMateriaNF = ['idCarrera', 'idMateria', 'descripcion', 'horas']
const fieldsAlumnx = ['idAlumnx', 'nombre', 'apellido', 'email', 'password', 'idPais', 'fechaNacimiento']
const fieldsPais = ['idPais', 'descripcion']
const fieldsPaisNF = ['idPais', 'descripcion', 'zonaHoraria']
const fieldsEventoTipo = ['idEventoTipo', 'descripcion']
const fieldsEventoTipoNF = ['idEventoTipo', 'descripcion', 'blablabla']
const fieldsAlumnxNF = ['idAlumnx', 'nombre', 'apellido', 'email', 'password', 'idPais', 'fechaNacimiento', 'grupoSanguineo']
const fieldsInstitucionNF = ['idAlumnx', 'idInstitucion', 'descripcion', 'domicilio']

module.exports = {
    fieldsAlumnx,
    fieldsCarrera,
    fieldsCarreraNF,
    fieldsCarreraByAlumnx,
    fieldsEvento,
    fieldsEventoNF,
    fieldsEventoByIdAlumnx,
    fieldsEventoByIdMateria,
    fieldsInstitucion,
    fieldsMateria,
    fieldsMateriaNF,
    fieldsPais,
    fieldsPaisNF,
    fieldsEventoTipo,
    fieldsEventoTipoNF,
    fieldsAlumnxNF,
    fieldsInstitucionNF
}